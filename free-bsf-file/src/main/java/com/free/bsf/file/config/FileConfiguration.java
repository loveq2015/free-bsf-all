package com.free.bsf.file.config;

import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.file.FileException;
import com.free.bsf.file.FileProvider;
import com.free.bsf.file.impl.AliOssStsFileProvider;
import com.free.bsf.file.impl.QiniuFileProvider;
import com.free.bsf.core.config.BsfConfiguration;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.file.impl.AbstractFileProvider;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.util.StringUtils;


/**
 * @author Huang Zhaoping
 **/
@Configuration
@Import(BsfConfiguration.class)

@ConditionalOnProperty(name = "bsf.file.enabled", havingValue = "true")
public class FileConfiguration implements InitializingBean {

    @Bean
    @Lazy
    public FileProvider fileProvider() {
        FileProvider fileProvider = null;
        if (StringUtils.isEmpty(FileProperties.getProvider())||
                FileProperties.PROVIDER_QINIU.equalsIgnoreCase(FileProperties.getProvider())||
                FileProperties.PROVIDER_ALIOSSSTS.equalsIgnoreCase(FileProperties.getProvider())) {
            // 未配置提供者的情况根据实际配置来自动选择
            if (FileProperties.getProvider().equals(FileProperties.PROVIDER_QINIU)) {
                fileProvider= new QiniuFileProvider();
            } else if(FileProperties.getProvider().equals(FileProperties.PROVIDER_ALIOSSSTS)) {
                fileProvider = new AliOssStsFileProvider();
            }else {
                throw new FileException("文件服务配置未提供");
            }

        } else {
            throw new FileException("不支持的文件服务提供者：" + FileProperties.getProvider());
        }
        LogUtils.info(FileConfiguration.class, FileProperties.Project, "已启动,"+((AbstractFileProvider)fileProvider).info());
        return fileProvider;
    }

    @Override
    public void afterPropertiesSet() {
        LogUtils.info(FileConfiguration.class, CoreProperties.Project, "已启动");
    }
}
