package com.free.bsf.file;

import com.free.bsf.file.base.UploadToken;

import java.io.InputStream;
/**
 * @author Huang Zhaoping
 * @edit chejiangyi
 */
public interface FileProvider {

    /**
     * 上传文件
     *
     * @param input 文件流
     * @param name  文件名
     * @return
     */
    String upload(InputStream input, String name);

    /**
     * 	上传文件
     *
     * @param filePath 上传的本地文件名
     */
    String upload(String filePath);
    /**
     * 上传临时文件
     *
     * @param input 文件流
     * @param name  文件名
     * @return
     */
    String uploadTemp(InputStream input, String name);
    /**
     * 	上传文件
     *
     * @param filePath 上传的本地文件名
     */
    String uploadTemp(String filePath);

    /**
     * 上传token
     * @return
     */
    UploadToken uploadToken(String name);

    /**
     * 删除文件
     *
     * @param url
     * @return
     */
    boolean delete(String url);
}
