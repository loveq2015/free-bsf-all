package com.free.bsf.file.base;

import lombok.Data;

/**
 * @Classname UploadToken
 * @Description
 * @Date 2021/4/26 16:19
 * @Created by chejiangyi
 */
@Data
public class UploadToken {
     /**
      * 第三方token
      */
     String token;
     /**
      * 自动生成的文件key
      */
     String key;
     /**
      * 上传的域名地址
      */
     String domain;
}
