package com.free.bsf.file.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.auth.sts.AssumeRoleRequest;
import com.aliyuncs.auth.sts.AssumeRoleResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.free.bsf.core.base.Callable;
import com.free.bsf.core.util.JsonUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.file.FileException;
import com.free.bsf.file.base.AliStsToken;
import com.free.bsf.file.base.UploadToken;
import com.free.bsf.file.config.AliOssStsProperties;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @description:
 * https://help.aliyun.com/document_detail/100624.htm?spm=a2c4g.11186623.0.0.13415b783quqWO#concept-xzh-nzk-2gb
 * https://www.alibabacloud.com/help/zh/resource-access-management/latest/faq-about-ram-roles-and-sts-tokens
 * @Author YH
 * @Date 2021/12/3 17:47
 */
public class AliOssStsFileProvider extends AbstractFileProvider {

    protected OSS ossClient;
    protected IAcsClient iAcsClient;
    protected String region;
    protected String roleArn;
    protected String roleSessionName;
    protected String endpoint;
    protected String accessKeyId;
    protected String accessKeySecret;
    protected String bucketName;
    protected String path;
    protected String bucketUrl;
    protected Long timeOut;

    public AliOssStsFileProvider() {
        if (StringUtils.isEmpty(AliOssStsProperties.getAccessKeyId())) {
            throw new FileException("阿里oss文件服务缺少AccessKeyId配置");
        }
        if (StringUtils.isEmpty(AliOssStsProperties.getAccessKeySecret())) {
            throw new FileException("阿里oss文件服务缺少AccessKeySecret配置");
        }
        if (StringUtils.isEmpty(AliOssStsProperties.getEndpoint())) {
            throw new FileException("阿里oss文件服务缺少Endpoint配置");
        }
        if (StringUtils.isEmpty(AliOssStsProperties.getBucketName())) {
            throw new FileException("阿里oss文件服务缺少BucketName配置");
        }
        if (StringUtils.isEmpty(AliOssStsProperties.getRegion())) {
            throw new FileException("阿里oss文件服务缺少Region配置");
        }
        if (StringUtils.isEmpty(AliOssStsProperties.getRoleArn())) {
            throw new FileException("阿里oss文件服务缺少RoleArn配置");
        }
        if (StringUtils.isEmpty(AliOssStsProperties.getRoleSessionName())) {
            throw new FileException("阿里oss文件服务缺少RoleSessionName配置");
        }
        if (AliOssStsProperties.getTimeOut()<=0) {
            throw new FileException("阿里oss文件服务缺少TimeOut配置");
        }
        this.timeOut = AliOssStsProperties.getTimeOut();
        path = AliOssStsProperties.getTempDir();
        //路径初始化
        if (StringUtils.isEmpty(path)) {
            path = "tmp/";
        }
        this.accessKeyId = AliOssStsProperties.getAccessKeyId();
        this.accessKeySecret = AliOssStsProperties.getAccessKeySecret();
        //oss-cn-hangzhou.aliyuncs.com
        this.endpoint = AliOssStsProperties.getEndpoint();
        //桶名称
        this.bucketName = AliOssStsProperties.getBucketName();
        //cn-hangzhou
        this.region = AliOssStsProperties.getRegion();
        //RAM角色的ARN acs:ram::1069735822596087:role/oss-file
        this.roleArn = AliOssStsProperties.getRoleArn();
        //扮演临时角色名称 自定义
        this.roleSessionName = AliOssStsProperties.getRoleSessionName();
        //初始化STS客户端
        DefaultProfile profile = DefaultProfile.getProfile(region, accessKeyId, accessKeySecret);
        //STS 客户端初始化
        this.iAcsClient = new DefaultAcsClient(profile);
        //OSS 客户端初始化

        this.ossClient = new OSSClientBuilder().build(endpoint,accessKeyId,accessKeySecret);
        String bucketUrl = AliOssStsProperties.getBucketUrl();
        if(StringUtils.isEmpty(bucketUrl)){
            bucketUrl = "https://" + bucketName + "." + endpoint + "/";
        }
        if (!bucketUrl.endsWith("/")) {
            bucketUrl += "/";
        }
        this.bucketUrl = bucketUrl;
    }

        private void getTokenOssClient(Callable.Action1<OSS> action1){
            OSS ossClient = null;
            try {
                UploadToken token = uploadToken(null);
                AliStsToken stsToken = JsonUtils.deserialize(token.getToken(),AliStsToken.class);
                ossClient = new OSSClientBuilder()
                        .build(endpoint, stsToken.getAccessKeyId(),
                                stsToken.getAccessKeySecret(),
                                stsToken.getSecurityToken());
                action1.invoke(ossClient);
            } catch (Exception e) {
                throw throwException(e);
            } finally {
                if(ossClient!=null) {
                    ossClient.shutdown();
                }
            }
        }

    /**
     * 阿里云OSS文件上传 非STS
     * @param input
     * @param name
     * @return
     */
    @Override
    public String upload(InputStream input, String path, String name) {
        try {
            //上传至默认地址
            String filePath = createFileKey(path, name);
            getTokenOssClient((oss)->{
                oss.putObject(bucketName,filePath,input);
            });
            return bucketUrl + filePath;
        } catch (Exception e) {
            throw throwException(e);
        }
    }

    @Override
    public String upload(String filePath, String path, String name) {
        try {
            getTokenOssClient((oss)->{
                oss.putObject(bucketName, createFileKey(path, name), new File(filePath));
            });
            return bucketUrl+ filePath;
        } catch (Exception e) {
            throw  throwException(e);
        }
    }

    /**
     * 获取临时角色的token和accessKeyId accessKeySecret
     *
     * @return
     */
    @Override
    public UploadToken uploadToken(String name) {
        AssumeRoleRequest request = new AssumeRoleRequest();
        request.setRegionId(region);
        request.setRoleArn(roleArn);
        request.setRoleSessionName(roleSessionName);
        request.setDurationSeconds(timeOut);
        try {
            AssumeRoleResponse response = iAcsClient.getAcsResponse(request);
            AssumeRoleResponse.Credentials credentials = response.getCredentials();
            //将STS返回的临时accessKeyId和accessKeySecret填入
            AliStsToken stsToken = new AliStsToken();
            stsToken.setSecurityToken(credentials.getSecurityToken());
            stsToken.setAccessKeyId(credentials.getAccessKeyId());
            stsToken.setAccessKeySecret(credentials.getAccessKeySecret());
            String token = JsonUtils.serialize(stsToken);
            UploadToken uploadToken = new UploadToken();
            uploadToken.setToken(token);
            uploadToken.setKey(createFileKey(null, name));
            uploadToken.setDomain(bucketUrl);
            return uploadToken;
        } catch (Exception e) {
            throw throwException(e);
        }
    }
    /**
     * 阿里云OSS单文件删除
     * @param url
     * @return
     */
    @Override
    public boolean delete(String url) {
        String path;
        try {
            path = new URL(url).getPath();
            if (path.startsWith("/")) {
                path = path.substring(1);
            }
        } catch (MalformedURLException e) {
            throw new FileException("URL错误：" + url, e);
        }
        try {
            ossClient.deleteObject(bucketName, path);
            return true;
        } catch (OSSException e) {
            return false;
        }
    }

    @Override
    public String info() {
        return "阿里OSS云文件服务:" + com.free.bsf.core.util.StringUtils.nullToEmpty(endpoint);
    }

    private FileException throwException(Exception e){
        if(e instanceof FileException){
            return  (FileException) e;
        }else{
            return new FileException("阿里OSS上传文件服务:"+e.getMessage(), e);
        }
    }
}
