# Cat集成说明

### 介绍
CAT(Central Application Tracking)基于Java开发的实时监控平台，主要包括移动端监控，应用侧监控，核心网络层监控，系统层监控等。

CAT是一个提供实时监控报警，应用性能分析诊断的工具。[更多详情](https://github.com/dianping/cat)。

本系统用于集成CAT并统一封装监控埋点，对业务无侵入实现监控。

### 技术选型
[技术选型](doc/README-选型.md) [暂无]

### 入门篇
```
#启用cat
bsf.cat.enabled=true
```

### 进阶篇
[进阶篇](doc/README-高级.md)