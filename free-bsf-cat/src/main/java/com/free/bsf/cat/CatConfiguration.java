package com.free.bsf.cat;


import com.free.bsf.cat.remote.CatFeginRequestInterceptor;
import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.util.PropertyUtils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import com.dianping.cat.Cat;
import com.free.bsf.core.base.BsfEnvironmentEnum;
import com.free.bsf.core.config.BsfConfiguration;
import com.free.bsf.core.util.LogUtils;

/**
 * @author: chejiangyi
 * @version: 2019-05-27 17:18
 **/
@org.springframework.context.annotation.Configuration
@ConditionalOnProperty(name = "bsf.cat.enabled", havingValue = "true")
@Import(value = {BsfConfiguration.class,CatMybatisPluginConfiguration.class})
public class CatConfiguration implements InitializingBean {
    
    @Override
    public void afterPropertiesSet() throws Exception {
        var catServerUrl = CatProperties.catServerUrl();
        if(catServerUrl.length()==0)
        {
            throw new BsfException("cat.server.url未配置");
        }

        Cat.initializeByDomain(PropertyUtils.getPropertyCache(CatProperties.SpringApplicationName,""),catServerUrl.split(","));
        LogUtils.info(CatConfiguration.class,CatProperties.Project,"已启动!!!"+" "+CatProperties.CatServerUrl+"="+ catServerUrl);
    }

    @Bean
    @ConditionalOnProperty(name = "bsf.cat.filter", havingValue = "cross")
    public CatFeginRequestInterceptor catFeginRequestInterceptor(){
        return new CatFeginRequestInterceptor();
    }
}
