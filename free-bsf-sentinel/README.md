## Sentinel集成说明
### 介绍
Sentinel: 分布式系统的流量防卫兵
* https://sentinelguard.io/zh-cn/docs/ 
* https://github.com/alibaba/Sentinel/wiki/%E4%BB%8B%E7%BB%8D


### 依赖引用
一般包已经通过free-bsf-starter依赖在脚手架项目中,无需额外配置
``` 
<dependency>
	<artifactId>free-bsf-sentinel</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7.1-SNAPSHOT</version>
</dependency>
```

### 配置说明
```
## bsf sentinel  集成
##sentinel服务的启用开关，[必须]，默认false
bsf.sentinel.enabled=false

#sentinel dashboard 地址,[必须]，默认为空
csp.sentinel.dashboard.server=localhost:8080

#bsf sentinel web 允许限流,默认开启
bsf.sentinel.web.enabled=true

#web 允许限流匹配,*类似like匹配，[必须]，默认为空,*表示所有
bsf.sentinel.web.includeResource=*

# bsf sentinel feign 允许限流，默认开启
bsf.sentinel.feign.enabled=true

#feign 允许限流匹配,*类似like匹配，[必须]，默认为空,*表示所有
bsf.sentinel.feign.includeResource=*

#flowRule规则配置,1...n,可以配置多个,默认为空,限流规则参考: https://sentinelguard.io/zh-cn/docs/basic-api-resource-rule.html
bsf.sentinel.flowRule1={"resource":"","limitApp":"default","grade":1,"count":0.0,"strategy":0,"controlBehavior":0,"warmUpPeriodSec":10,"maxQueueingTimeMs":500,"clusterMode":false}		
bsf.sentinel.degradeRule1={"resource":"","grade":0,"count":0.0,"timeWindow":0,"minRequestAmount":5,"slowRatioThreshold":1.0,"statIntervalMs":1000}
bsf.sentinel.systemRule1={"highestSystemLoad":-1.0,"highestCpuUsage":-1.0,"qps":-1.0,"avgRt":-1,"maxThread":-1}
bsf.sentinel.authorityRule1={"resource":"","strategy":0}

# bsf sentinel @SentinelResource 注解支持限流，默认开启
bsf.sentinel.resourceAspect.enabled=true
```

### rule详细说明
#### 流量控制FlowRule

|      Field      | 说明                                                         | 默认值               |
| :-------------: | :----------------------------------------------------------- | :------------------- |
|    resource     | 资源名，资源名是限流规则的作用对象，如"/attribute/list"      |                      |
|      count      | 限流阈值                                                     | 0.0                  |
|      grade      | 限流阈值类型，QPS-0 或线程数模式-1                           | QPS                  |
|    limitApp     | 流控针对的调用来源，default-表示不区分调用来源、{some_origin_name}-表示只对该调用者的请求进行流量控制、other-表示对除了{some_origin_name}的请求进行流量控制 | `default`            |
|    strategy     | 调用关系限流策略：直接-0、链路-2、关联-1                     | 根据资源本身（直接） |
| controlBehavior | 流控效果（直接拒绝-0 / 排队等待-1 / 慢启动模式-2），不支持按调用关系限流 | 直接拒绝             |

#### 熔断降级规则 (DegradeRule)

|       Field        | 说明                                                         | 默认值     |
| :----------------: | :----------------------------------------------------------- | :--------- |
|      resource      | 资源名，即规则的作用对象                                     |            |
|       grade        | 熔断策略，支持慢调用比例-0/异常比例-1/异常数策略-2           | 慢调用比例 |
|       count        | 慢调用比例模式下为慢调用临界 RT（超出该值计为慢调用）；异常比例/异常数模式下为对应的阈值 |            |
|     timeWindow     | 熔断时长，单位为 s                                           |            |
|  minRequestAmount  | 熔断触发的最小请求数，请求数小于该值时即使异常比率超出阈值也不会熔断（1.7.0 引入） | 5          |
|   statIntervalMs   | 统计时长（单位为 ms），如 60*1000 代表分钟级（1.8.0 引入）   | 1000 ms    |
| slowRatioThreshold | 慢调用比例阈值，仅慢调用比例模式有效（1.8.0 引入）           |            |

#### 系统保护规则 (SystemRule)

|       Field       | 说明                                   | 默认值      |
| :---------------: | :------------------------------------- | :---------- |
| highestSystemLoad | `load1` 触发值，用于触发自适应控制阶段 | -1 (不生效) |
|       avgRt       | 所有入口流量的平均响应时间             | -1 (不生效) |
|     maxThread     | 入口流量的最大并发数                   | -1 (不生效) |
|        qps        | 所有入口资源的 QPS                     | -1 (不生效) |
|  highestCpuUsage  | 当前系统的 CPU 使用率（0.0-1.0）       | -1 (不生效) |

#### 访问控制规则 (AuthorityRule)

|  Field   | 说明                                 | 默认值                        |
| :------: | :----------------------------------- | :---------------------------- |
| resource | 资源名，即规则的作用对象             |                               |
| strategy | 限制模式，白名单模式-0，黑名单模式-1 | 白名单模式                    |
| limitApp | 流控针对的调用来源                   | `default`，代表不区分调用来源 |

### 使用说明
```
    #注解方式限流 写法参考文档: https://github.com/alibaba/Sentinel/wiki/%E6%B3%A8%E8%A7%A3%E6%94%AF%E6%8C%81
    // 原函数
    @SentinelResource(value = "hello", blockHandler = "exceptionHandler", fallback = "helloFallback")
    public String hello(long s) {
        return String.format("Hello at %d", s);
    }
    
    // Fallback 函数，函数签名与原函数一致或加一个 Throwable 类型的参数.
    public String helloFallback(long s) {
        return String.format("Halooooo %d", s);
    }

    // Block 异常处理函数，参数最后多一个 BlockException，其余与原函数一致.
    public String exceptionHandler(long s, BlockException ex) {
        // Do some log here.
        ex.printStackTrace();
        return "Oops, error occurred at " + s;
    }

    // 这里单独演示 blockHandlerClass 的配置.
    // 对应的 `handleException` 函数需要位于 `ExceptionUtil` 类中，并且必须为 public static 函数.
    @SentinelResource(value = "test", blockHandler = "handleException", blockHandlerClass = {ExceptionUtil.class})
    public void test() {
        System.out.println("Test");
    }
```
### 选型 
![Image text](doc/选型.png)
### 高级篇 
#### 原理说明 
通过sentinel基础的demo限流方式与apollo集成，支持feign和web请求限流。与市面上常规的限流apollo集成不同，bsf底层是自研集成apollo的，所以配置方式也不同。
为了尽可能少的减少sentinel对业务的性能影响，故引入includeResource过滤部分请求，避开sentinel限流能力。而dashboard核心的定位，依然在于监控能力和有限的限流控制。
* 持久化配置:
所有的限流持久化与apollo打通，可以做到apollo配置修改，实时推送到应用端。
* 临时限流配置:
可以通过dashboard配置，在应用服务或者dashboard未重启的时候生效，重启后失效。
#### 其他
本集成未严格验证测试.

