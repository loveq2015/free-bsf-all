# free-bsf-all

## 源起
起源 https://gitee.com/yhcsx/csx-bsf-all (现已停止开源)项目,重新定制化修改。

## 介绍
BSF 为 Base Service Framework 的简写,定义为技术团队的基础框架,用于基础服务的集成和跟业务无关的基础技术集成。
BSF 集成并封装Apollo,Rocket MQ,Redis, Elastic Search,ELK,XXL-JOB, Sharding JDBC,Cat,Eureka,七牛云,Swagger等第三方中间件,标准化使用规范,并集成了自研的监控报警,用来监控各个服务系统的性能及异常告警,提供简易使用的底层框架。

## 愿景
【技术一体化】为了更好地支持业务开发，让开发人员从中间件中解放出来，专注业务以提高开发效率。同时基础框架集中统一优化中间件相关服务及使用，为开发人员提供高性能,更方便的基础服务接口及工具，实践过程可让效率提升10倍。

## 项目结构规范说明

```
free-bsf-all 
    -- free-bsf-core (项目核心类库)
    -- free-bsf-demo (项目集成使用demo)
    -- free-bsf-dependencies (项目依赖pom定义)
        -- README.md (说明文档，必须有)
    -- free-bsf-starter （项目full-start包）
    -- free-bsf-elk (ELK集成)
    -- free-bsf-job (XXL-JOB集成)
    -- free-bsf-cat (CAT监控集成)
    -- free-bsf-apollo (Apollo配置中心集成)
    -- free-bsf-message (消息-短信-钉钉消息集成)
    -- free-bsf-shardingjdbc (分库分表ShardingJDBC 集成) 
    -- free-bsf-mq (消息队列Rocket MQ集成) 
    -- free-bsf-redis(缓存Redis集成)
    -- free-bsf-eureka(服务注册与发现集成)
    -- free-bsf-file（文件服务集成）
    -- free-bsf-elasticsearch(ES集成) 
    -- free-bsf-health（自研健康检查） 
    -- free-bsf-transaction（努力送达事务） 
    -- free-bsf-autotest（自动化测试流量录制） 
    -- free-bsf-apiregistry（自研api注册中心） 
    -- 框架名 (例如:free-bsf-elk,cat,apollo等)
```

## 相关文档
本系统个子模块分别集成分装了对应中间件服务，文档如下：

1. [free-bsf-core](free-bsf-core/README.md)
2. [free-bsf-demo](free-bsf-demo/README.md)
3. [free-bsf-dependencies](free-bsf-dependencies/README.md)
4. [free-bsf-starter](free-bsf-starter/README.md)
5. [free-bsf-elk](free-bsf-elk/README.md)
6. [free-bsf-job](free-bsf-job/README.md)
7. [free-bsf-cat](free-bsf-cat/README.md)
8. [free-bsf-apollo](free-bsf-apollo/README.md)
9. [free-bsf-message](free-bsf-message/README.md)
10. [free-bsf-shardingjdbc](free-bsf-shardingjdbc/README.md)
11. [free-bsf-mq](free-bsf-mq/README.md)
12. [free-bsf-redis](free-bsf-redis/README.md)
13. [free-bsf-eureka](free-bsf-eureka/README.md)
14. [free-bsf-file](free-bsf-file/README.md)
15. [free-bsf-elasticsearch](free-bsf-elasticsearch/README.md)
16. [free-bsf-health](free-bsf-health/README.md)
17. [free-bsf-transaction](free-bsf-transaction/README.md)
18. [free-bsf-sentinel](free-bsf-sentinel/README.md)
19. [free-bsf-autotest](free-bsf-autotest/README.md)
20. [free-bsf-apiregistry](free-bsf-apiregistry/README.md)

## 编译说明
1. 首次下载代码,首先构建free-bsf-dependencies模块，然后在构建整个工程。
2. [缺包无法编译](resources/README-缺包无法编译.md)，请下载resources下的[elasticsearch-sql-6.7.1.0.rar](resources/elasticsearch-sql-6.7.1.0.rar),[es-sql对druid v1.2.5+的改造支持](https://gitee.com/chejiangyi/elasticsearch-sql-for-druid/)

```
mvn install free-bsf-dependencies
mvn install free-bsf-all
```

## 版本升级/切换
```
## 备注: 格式:1.0-SNAPSHOT (版本号+-+RELEASE/SNAPSHOT) 
cd free-bsf-dependencies
mvn versions:set -DgenerateBackupPoms=false
或
mvn versions:set -DgenerateBackupPoms=false -DnewVersion={version}
```

## 使用说明

1. 依赖引用

```
    <!--引入依赖版本定义1方式-->
    <parent>
        <groupId>com.free.bsf</groupId>
        <artifactId>free-bsf-dependencies</artifactId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <!--引入依赖版本定义2方式-->
    <dependency>
        <groupId>com.free.bsf</groupId>
        <artifactId>free-bsf-dependencies</artifactId>
        <version>1.0-SNAPSHOT</version>
        <type>pom</type>
        <scope>import</scope>
    </dependency>
    <!--引入依赖 free-bsf-starter-->
    <dependency>
        <artifactId>free-bsf-starter</artifactId>
        <groupId>com.free.bsf</groupId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
```

2. Demo程序
   框架的使用demo，请参考[free-bsf-demo](free-bsf-demo)
   各个组件的使用，请参考相关模块文档。
3. 集成配置参考properties配置文件
   [resources/application.properties](resources/application.properties)

## 更新记录

##### 1.1-SNAPSHOT
* bsf 支持sentinel限流。
* bsf 支持transaction分布式事务seata。
* bsf 支持阿里云oss sts认证模式上传文件。
##### 1.2-SNAPSHOT
* bsf 支持自动化测试流量录制。
##### 1.3-SNAPSHOT
* bsf 支持[k8s不断服,滚动发布](https://www.cnblogs.com/chejiangyi/p/16808139.html )。
* bsf 切换新的maven仓库。
* bsf 环境变量换成dev,test,pre,pro为标准。
##### 1.4-SNAPSHOT
* bsf 支持autotest 自动化测试 AutoTestAttribute 注解,[全链路压测实践](https://www.cnblogs.com/chejiangyi/p/16900586.html )。
##### 1.5-SNAPSHOT
* bsf 支持autotest get请求bug修复。
##### 1.6-SNAPSHOT
* bsf 支持elk logstash动态限流能力。
##### 1.7-SNAPSHOT
* bsf 修復所有properties配置模糊匹配的問題。
##### 1.8-SNAPSHOT
* bsf health模块对sql和url报警支持。
##### 1.9-SNAPSHOT
* bsf springboot升级为2.3.12.RELEASE和springCloud升级为Hoxton.SR12 【内部测试未验证】
##### 2.1-SNAPSHOT
* bsf 基于1.9-SNAPSHOT,增加apiRegistry api注册中心组件。兼容旧feign的get,post通用写法。支持旧eureka rpc结果对比测试。
##### 2.2-SNAPSHOT
* bsf eureka模块移除掉,bsf整个eureka相关的依赖全部删除,增加启动APIRegistry bean扫描支持,增加health sql参数集和结果集过大报警支持。
##### 2.3-SNAPSHOT
* bsf shardingjdbc 升级为shardingsphere 5.0.0版本,支持影子表和读写分离等(备注:shardingjdbc 自身包管理比较乱,版本升级后导致引入大量包)
##### 2.4-SNAPSHOT
* bsf nacos支持对nacos配置中心使用,ApiRegistry注册中心支持nacos及rpc调用。修复BsfConfiguration中对bsf连接池和httpclient的严重bug。
* bsf autotest bug修复,可以支持表单提交录制。
##### 2.5-SNAPSHOT
* bsf redis支持限流:计数,滑动,令牌三种模式。
##### 3.0-SNAPSHOT
* bsf springboot升级为2.6.14和springCloud升级为2021.0.7 (jdk8)
##### 3.1-SNAPSHOT
* bsf springboot升级为2.6.14和springCloud升级为2021.0.7 (jdk17),mybatis-plus升级3.5.1(相应mybatis升级版本),swagger升级到3.0.0
  [升级踩坑笔记](resources/doc/jdk17及springboot-2-6-14升级.md)
##### 3.2-SNAPSHOT
* bsf es重写并升级,注解采用spring data elasticsearch(4.0.9)为主,其他保持接口最大程度兼容！ (jdk17) ,elasticsearch 版本为7.6.2!
##### 3.3-SNAPSHOT
* bsf redis原仅支持集群版本客户端,现增加支持单机版客户端使用,sdk对外接口保持兼容。
##### 3.4-SNAPSHOT
* bsf postgresql 驱动和mybatis plus代码生成支持。[pgsql兼容笔记](resources/doc/pgsql.md)
* 优化性能:跳过无必要的elasticsearch自动装配
## 参与贡献
架构师: [车江毅](https://gitee.com/chejiangyi)

## 参考文档
* BSF框架在研发管理体系中的应用-[五个维度打造研发管理体系](https://www.cnblogs.com/chejiangyi/p/15420637.html)
* BSF框架在研发架构体系中的应用-[高效能研发体系构建概论](https://www.cnblogs.com/chejiangyi/p/15000543.html)
* BSF框架在构建消息中心中的应用-[亿级消息中心架构方案概述](https://www.cnblogs.com/chejiangyi/p/14884931.html)
* BSF框架在业务架构体系中的应用-[永辉彩食鲜架构概述](https://www.cnblogs.com/chejiangyi/p/12089803.html)
* BSF框架在自动化测试中的应用-[全链路压测效能10倍提升的压测工具实践笔记](https://www.cnblogs.com/chejiangyi/p/16900586.html)

## 案例
![永辉](resources/logo/yh.jpg)
![长投学堂](resources/logo/changtou.jpeg)
![bitmart](resources/logo/bitmart.jpeg)
![柚凡](resources/logo/youfan.jpeg)
![领猫](resources/logo/linkmore.png)
![彩食鲜](resources/logo/caishixian.jpeg)
##### by 车江毅
