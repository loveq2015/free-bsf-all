# 自动化测试集成
简单自动化测试框架集成,可进行全链路线上流量录制。现支持mysql存储采样,未来支持es和kafka,rocketmq等。下个版本会考虑支持影子库。<br/>
压测框架:全链路压测工具(https://gitee.com/chejiangyi/lmc-autotest)

### 技术选型
* 参考阿里云pts,metersphere,yapi等工具。
* 参考有赞，阿里，美团等全链路压测实现思路。

### 技术实现
![bsf-autotest示意图](doc/autotest.png)

### 依赖引用
添加bsf模块到项目依赖中

``` 
<dependency>
	<artifactId>free-bsf-core</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7-SNAPSHOT</version>
</dependency>
<dependency>
	<artifactId>free-bsf-autotest</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7-SNAPSHOT</version>
</dependency>
```

### 入门篇
bsf配置
```
spring.application.name=free-demo-provider
#一键启用开关,默认false,重启后生效
autotest.enabled=false

#是否开启流量录制,默认false,实时开关生效
autotest.record.enabled=false

#录制请求包装类类型,支持AutoTestCachedRequestWrapper(仅支持json)或AutoTestContentCachingRequestWrapper(兼容多种数据)
autotest.record.wrapperType=AutoTestCachedRequestWrapper

#录制频率,默认为1,即1:1录制;<=0则默认不采集;实时生效
autotest.record.frequency=1

#录制跳过部分url,支持前后*模糊匹配，支持逗号分割多个url,默认为空。
autotest.record.skip.urls=

#录制批量缓存的最大数量,默认5000
autotest.record.cache.max=5000

#针对微服务,是否开启链路追求录制流量,仅支持feign调用http场景;开启后http header 中增加autotest-traceid头。
autotest.traceid.enabled=true

#录制流量时,自动检测操作类型是否为仅查询/操作,目前仅支持mybatis场景下的sql检测;如insert,update,delete被判别为操作类型。
autotest.operatortype.enabled=false

#录制流量存储持久化刷新的周期,单位ms,默认5000
autotest.store.flush.timespan=5000

#录制流量存储的保存类型,默认mysql,未来支持es,kafka,rocketmq
autotest.store.type=mysql

### mysql 存储引擎
autotest.store.mysql.driver=com.mysql.cj.jdbc.Driver
#注意开启:rewriteBatchedStatements=true
autotest.store.mysql.url=jdbc:mysql://{填入db信息}:3306/autotest?useSSL=false&serverTimezone=Asia/Shanghai&autoReconnect=true&rewriteBatchedStatements=true
autotest.store.mysql.user={填入账号}
autotest.store.mysql.password={填入密码}

#是否开启属性注解,开启后属性信息会同步到样本文件。
autotest.attribute.enabled=false
#bsf所在环境变量,一般配合属性注解使用
bsf.env=dev
```
bsf配置接口attribute注解
注解字段释义
* author 作者
* apiType api类型:无,操作,查询
* test 是否测试:无,需要,跳过
* level api级别:核心,重要,普通,次要,报表
* extMap 扩展注解,除非需要在管理端进行样本筛选时有独特要求,一般无需添加。格式:Map类型的json字符串。
```
//示例,注解信息会显示到autotest sample信息和压测报告中,不加注解也不影响api流量样本录制。
@PostMapping("/test")
@AutoTestAttribute(author = "川乌",apiType = Attributes.ApiTypeEnum.OPERATOR, test = Attributes.TestEnum.NEED,level = Attributes.LevelEnum.CORE, extMap = "{'aaa':'bbb'}")
public ApiResponseEntity<String>  test(String str) {
    return ApiResponseEntity.success("ok");
}
```


### 进阶篇
1. 暂不支持文件上传类的流量录制。
2. 微服务的流量录制目前支持feign进行traceid传递,支持mybatis进行操作或仅查询两种访问类型识别。
3. 期望通过sharding-jdbc支持影子表和影子库,未来还会在jdbc层面做影子表和影子库扩展能力。


by 车江毅