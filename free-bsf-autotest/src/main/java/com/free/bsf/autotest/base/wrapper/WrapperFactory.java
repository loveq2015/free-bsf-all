package com.free.bsf.autotest.base.wrapper;

import com.free.bsf.autotest.AutoTestProperties;
import com.free.bsf.core.util.PropertyUtils;
import lombok.val;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class WrapperFactory {
    public static HttpServletRequest create(HttpServletRequest request) throws IOException, ServletException {
        val wrapperType = PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestRecordWrapperType,"AutoTestCachedRequestWrapper");
        if(AutoTestCachedRequestWrapper.class.getSimpleName().equals(wrapperType)){
            return new AutoTestCachedRequestWrapper(request);
        }else if(AutoTestContentCachingRequestWrapper.class.getSimpleName().equals(wrapperType)){
            return new AutoTestContentCachingRequestWrapper(request);
        }
        return request;
    }

    public static boolean isWrapper(HttpServletRequest request) {
        return (request instanceof IRequestWrapper);
    }
}
