package com.free.bsf.autotest.base;

import com.free.bsf.core.util.WebUtils;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;

import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import java.util.*;


/**
 * @author: chejiangyi
 * @version: 2019-08-31 10:18
 * 来源第三方代码,整理修改
 * **/
@Intercepts({
    @Signature(method = "query", type = Executor.class, args = { MappedStatement.class, Object.class,
            RowBounds.class, ResultHandler.class }),
    @Signature(method = "query", type = Executor.class, args = { MappedStatement.class, Object.class,
            RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class}),
    @Signature(method = "update", type = Executor.class, args = { MappedStatement.class, Object.class })
})
public class AutoTestMybatisPlugin implements Interceptor{

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
        if(WebUtils.getRequest()!=null) {
            if (mappedStatement.getSqlCommandType() != SqlCommandType.SELECT) {
                AutoTestUtil.setOperatorType(WebUtils.getRequest(), OperatorTypeEnum.operator.getName());
            } else {
                AutoTestUtil.setOperatorType(WebUtils.getRequest(), OperatorTypeEnum.queryOnly.getName());
            }
        }

        return invocation.proceed();
    }


    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
    }
}
