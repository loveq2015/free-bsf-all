package com.free.bsf.autotest;

import com.free.bsf.autotest.base.AutoTestUtil;
import com.free.bsf.autotest.base.OperatorTypeEnum;
import com.free.bsf.autotest.base.wrapper.WrapperFactory;
import com.free.bsf.autotest.store.StoreManager;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.http.HttpStatus;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 流浪录制过滤器
 */
@Slf4j
public class AutoTestRecordFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        if(PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestRecordEnabled,false)
                &&!(ServletFileUpload.isMultipartContent(req))
                &&!AutoTestUtil.hitSkipUrl(req)&&!AutoTestUtil.isTestRunning(req)) {//
            try {
                if(AutoTestUtil.hitFrequency(req)) {
                    req  = WrapperFactory.create(req);
                    req.setAttribute(AutoTestProperties.AutoTestReWriteRequest,"true");
                }
                AutoTestUtil.setTraceId(req);
            } catch (Exception e) {
                LogUtils.warn(AutoTestRecordFilter.class, AutoTestProperties.Project, "[报警]录制流量出错", e);
            }
        }

        chain.doFilter(req, res);

        try {
            if (AutoTestUtil.hasReWriteRequest(req)) {
                val requestInfo = AutoTestUtil.getRequestInfo(req);
                //是否记录属性
                val attribute=req.getAttribute(AutoTestProperties.AutoTestAttribute);
                if(attribute!=null&&attribute instanceof String){
                    requestInfo.setAttribute((String)attribute);
                }
                //仅记录200请求
                if(res.getStatus()== HttpStatus.OK.value()) {
                    AutoTestContext.Context.getRequestCache().add(requestInfo);
                }
            }
            if (res != null) {
                //把操作类型传出去
                if (!OperatorTypeEnum.none.getName().equalsIgnoreCase(AutoTestUtil.getOperatorType(req))) {
                    res.setHeader(AutoTestProperties.AutoTestOperatorType, AutoTestUtil.getOperatorType(req));
                }
            }

        }catch (Exception e){
            LogUtils.warn(AutoTestRecordFilter.class, AutoTestProperties.Project, "[报警]录制流量出错", e);
        }
    }
    @Override
    public void destroy() {
    }
    @Override
    public void init(FilterConfig filterConfig) {
        try {
            StoreManager.Default.createOrGet().run();
        }catch (Exception e){
            LogUtils.error(AutoTestRecordFilter.class,AutoTestProperties.Project,"项目autotest采集启动失败");
            throw e;
        }
    }
}
