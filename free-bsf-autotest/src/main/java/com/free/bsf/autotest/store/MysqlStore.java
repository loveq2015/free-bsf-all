package com.free.bsf.autotest.store;

import com.free.bsf.autotest.AutoTestProperties;
import com.free.bsf.autotest.base.AutoTestException;
import com.free.bsf.autotest.base.RequestInfo;
import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.db.DbConn;
import com.free.bsf.core.db.DbHelper;
import com.free.bsf.core.util.DateUtils;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.ThreadUtils;
import lombok.val;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class MysqlStore extends BaseStore {
    private DbConn dbConn=null;
    private Object lock = new Object();
    private String sql="";
    public MysqlStore(){
        sql="INSERT INTO `{table}`(`url`, `app_name`, `header`, `body`, `create_time`, `fromip`, `traceid`,`trace_top`,`method`,`operator_type`,`attribute`) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }

    @Override
    protected void innerRun(RequestInfo[] requestInfos) {
        checkConn();
        if(dbConn!=null) {
            String table = "auto_tb_sample_"+ LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy_MM_dd"));
            if(!tableIsExist(table)){
                tryCreateTable(table);
                if(!tableIsExist(table)) {
                    LogUtils.error(MysqlStore.class, AutoTestProperties.Project, "[报警]流量采集表" + table + "未创建", null);
                    return;
                }
            }
            try {
                dbConn.beginTransaction(1);
                Object[][] ps = new Object[requestInfos.length][];
                for(int i=0;i<requestInfos.length;i++){
                    val info = requestInfos[i];
                    ps[i]=new Object[]{
                            info.getUrl(),
                            info.getAppName(),
                            info.getHeader(),
                            info.getBody(),
                            info.getCreateTime(),
                            info.getFromIp(),
                            info.getTraceId(),
                            info.getTraceTop(),
                            info.getMethod(),
                            info.getOperatorType(),
                            info.getAttribute()
                    };
                }
                dbConn.batch(sql.replace("{table}",table),ps);
                dbConn.commit();
            } catch (Exception e) {
                LogUtils.error(MysqlStore.class, AutoTestProperties.Project, "[报警]流量采集数据批量上传出错", e);
                dbConn.rollback();
            }
        }
    }

    protected void checkConn(){
        if(dbConn!=null)
            return;
        synchronized (lock) {
            if (dbConn == null)
                if(!PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestStoreMysqlUrl, "").toLowerCase().contains("rewriteBatchedStatements".toLowerCase())){
                    throw new AutoTestException("autotest.store.mysql.url配置未开启rewriteBatchedStatements=true",null);
                }
                dbConn = new DbConn(
                        PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestStoreMysqlUrl, ""),
                        PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestStoreMysqlUser, ""),
                        PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestStoreMysqlPassword, ""),
                        PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestStoreMysqlDriver, "")
                );
        }
    }

    protected boolean tableIsExist(String name){
        String sql="SHOW TABLES like ? ";
        val ds = dbConn.executeList(sql,new Object[]{name});
        if(ds!=null&&ds.size()>0){
            return true;
        }
        return false;
    }

    protected void tryCreateTable(String name){
        int tryCount=0;
        while (tryCount<2) {
            try {
                dbConn.executeSql("CREATE TABLE " + name + " LIKE tb_sample_example", new Object[]{});
                return;
            } catch (Exception e) {
                if(tryCount>=1) {
                    LogUtils.error(this.getClass(), AutoTestProperties.Project, "生成采样表失败", e);
                }
            }
            ThreadUtils.sleep(1000);
            tryCount++;
        }
    }

    @Override
    public void close() {
        super.close();
        if(dbConn!=null) {
            dbConn.close();
        }
    }
}
