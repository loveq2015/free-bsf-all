package com.free.bsf.autotest.base.wrapper;

import com.free.bsf.autotest.base.AutoTestIOUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.Getter;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.Part;
import java.io.*;
import java.util.Collection;

/**
 * 自己实现,不支持文件上传和表单提交,仅支持json
 */
public class AutoTestCachedRequestWrapper extends HttpServletRequestWrapper implements IRequestWrapper,Closeable {
    byte[] body;
    Collection<Part> parts;
    boolean isMultipartContent;
    public AutoTestCachedRequestWrapper(HttpServletRequest request) throws IOException, ServletException {
        super(request);
        //request.setAttribute("bsf_request_wrapper",this);
        isMultipartContent = ServletFileUpload.isMultipartContent(request);
        if(isMultipartContent){
            parts=request.getParts();
        }else {
            body = AutoTestIOUtils.toArrays(request.getInputStream());
        }
    }
    public byte[] getBody(){
        return body;
    }
    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }
    @Override
    public ServletInputStream getInputStream() throws IOException
    {
        return new BufferedServletInputStream(body);
    }

    @Override
    public Collection<Part> getParts() throws IOException, ServletException {
        if(isMultipartContent) {
            return parts;
        }
        return super.getParts();
    }

    public void close(){
        body=null;
    }

    private class BufferedServletInputStream extends ServletInputStream {
        private byte[] body;
        private int lastIndexRetrieved = -1;
        private ReadListener listener;

        public BufferedServletInputStream(byte[] body) {
            this.body = body;
        }

        @Override
        public int read() throws IOException {
            if (isFinished()) {
                return -1;
            }
            int i = body[lastIndexRetrieved + 1];
            lastIndexRetrieved++;
            if (isFinished() && listener != null) {
                try {
                    listener.onAllDataRead();
                } catch (IOException e) {
                    listener.onError(e);
                    throw e;
                }
            }
            return i;
        }

        @Override
        public boolean isFinished() {
            return lastIndexRetrieved == body.length - 1;
        }

        @Override
        public boolean isReady() {
            return isFinished();
        }

        @Override
        public void setReadListener(ReadListener listener) {
            if (listener == null) {
                throw new IllegalArgumentException("listener cann not be null");
            }
            if (this.listener != null) {
                throw new IllegalArgumentException("listener has been set");
            }
            this.listener = listener;
            if (!isFinished()) {
                try {
                    listener.onAllDataRead();
                } catch (IOException e) {
                    listener.onError(e);
                }
            } else {
                try {
                    listener.onAllDataRead();
                } catch (IOException e) {
                    listener.onError(e);
                }
            }
        }

        @Override
        public int available() throws IOException {
            return body.length - lastIndexRetrieved - 1;
        }

        @Override
        public void close() throws IOException {
            lastIndexRetrieved = body.length - 1;
            body = null;
        }
    }
}
