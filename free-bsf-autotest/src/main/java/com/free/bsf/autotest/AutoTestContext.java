package com.free.bsf.autotest;

import com.free.bsf.autotest.base.RequestCache;
import lombok.Data;

import java.util.concurrent.atomic.AtomicLong;

@Data
public class AutoTestContext {
    public static AutoTestContext Context = new AutoTestContext();
    /**
     * 访问捕获的样本的缓存
     */
    private RequestCache requestCache = new RequestCache();
    /**
     * 访问次数记录
     */
    private AtomicLong recordCount = new AtomicLong();
}
