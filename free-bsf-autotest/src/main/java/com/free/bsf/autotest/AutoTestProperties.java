package com.free.bsf.autotest;

public class AutoTestProperties {
    /**
     * Fields
     */
    public static final String Project="AutoTest";
    public static final String AutoTestTraceId="autotest-traceid";
    public static final String AutoTestOperatorType="autotest-operatortype";
    public static final String AutoTestRunning="autotest-running";
    public static final String AutoTestAttribute="autotest-attribute";
    public static final String AutoTestReWriteRequest="autotest-rewrite-request";

    //是否开启自动化测试
    public static final String AutoTestEnabled="autotest.enabled";
    //是否开启录制,默认false
    public static final String AutoTestRecordEnabled="autotest.record.enabled";
    //录制请求包装类类型
    public static final String AutoTestRecordWrapperType="autotest.record.wrapperType";
    //录制频率,默认为1
    public static final String AutoTestRecordFrequency="autotest.record.frequency";
    //录制跳过url路径,前后*代表模糊匹配,逗号分割支持多个url,默认为空
    public static final String AutoTestRecordSkipUrls="autotest.record.skip.urls";
    //录制缓存的最大数量,默认5000
    public static final String AutoTestRecordCacheMax = "autotest.record.cache.max";
    //是否开启traceid传递,默认true
    public static final String AutoTestTraceIdEnabled="autotest.traceid.enabled";
    //是否开启queryOnly检测,默认true
    public static final String AutoTestOperatorTypeEnabled="autotest.operatortype.enabled";

    //存储保存的周期,单位ms,默认5000
    public static final String AutoTestStoreFlushTimeSpan="autotest.store.flush.timespan";
    //存储的保存类型,默认mysql,未来支持es,kafka,rocketmq
    public static final String AutoTestStoreType = "autotest.store.type";
    //mysql存储引擎
    public static final String AutoTestStoreMysqlDriver="autotest.store.mysql.driver";
    public static final String AutoTestStoreMysqlUrl="autotest.store.mysql.url";
    public static final String AutoTestStoreMysqlUser="autotest.store.mysql.user";
    public static final String AutoTestStoreMysqlPassword="autotest.store.mysql.password";
    //开启属性记录
    public static final String AutoTestAttributeEnabled="autotest.attribute.enabled";

}
