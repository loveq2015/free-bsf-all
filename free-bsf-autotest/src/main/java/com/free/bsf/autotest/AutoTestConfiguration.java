package com.free.bsf.autotest;

import com.free.bsf.autotest.base.*;
import com.free.bsf.autotest.base.attr.AutoTestAttributeAspect;
import com.free.bsf.core.config.BsfConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.*;

@Configuration
@ConditionalOnProperty(name = AutoTestProperties.AutoTestEnabled, havingValue = "true")
@Import({BsfConfiguration.class,AutoTestConfiguration.FeignRequestInterceptorConfiguration.class})
public class AutoTestConfiguration {
    @Bean
    public AutoTestRecordFilter autoTestRecordFilter() {
        return new AutoTestRecordFilter();
    }

    @Bean
    @ConditionalOnProperty(name = AutoTestProperties.AutoTestOperatorTypeEnabled, havingValue = "true")
    @ConditionalOnClass(name = "feign.codec.Decoder")
    public FeignClientResponseAspect autoTestFeignClientResponseAspect() {
        return new FeignClientResponseAspect();
    }

    @Bean
    @ConditionalOnProperty(name = AutoTestProperties.AutoTestOperatorTypeEnabled, havingValue = "true")
    @ConditionalOnClass(name = {"org.apache.ibatis.plugin.Interceptor"})
    public AutoTestMybatisPlugin autoTestMybatisPlugin() {
        return new AutoTestMybatisPlugin();
    }

    /*解决包依赖自动被扫描的问题*/
    @Configuration
    @ConditionalOnProperty(name = AutoTestProperties.AutoTestTraceIdEnabled, havingValue = "true")
    @ConditionalOnClass(name = "feign.RequestInterceptor")
    public static class FeignRequestInterceptorConfiguration{
        @Bean
        public FeignRequestInterceptor autoTestFeignRequestInterceptor() {
            return new FeignRequestInterceptor();
        }
    }


    @Bean
    @ConditionalOnProperty(name = AutoTestProperties.AutoTestAttributeEnabled, havingValue = "true")
    public AutoTestAttributeAspect autoTestAttributeAspect() {
        return new AutoTestAttributeAspect();
    }
}
