package com.free.bsf.autotest.base;

import com.free.bsf.core.base.Environment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public enum OperatorTypeEnum {
    queryOnly("仅查询"),
    operator("操作"),
    none("未知");
    @Getter @Setter
    private String name;
}
