package com.free.bsf.autotest.base;

import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.db.DbConn;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;

public class AutoTestException extends BsfException {
    public AutoTestException(String message,Exception exp){
        super(message, exp);
    }
}
