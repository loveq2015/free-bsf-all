package com.free.bsf.autotest.base.wrapper;

import com.free.bsf.core.util.StringUtils;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;

/**
 * springboot的实现,支持表单提交和json
 */
public class AutoTestContentCachingRequestWrapper extends ContentCachingRequestWrapper implements IRequestWrapper {
    public AutoTestContentCachingRequestWrapper(HttpServletRequest request) {
        super(request);
    }
    public byte[] getBody(){
        return this.getContentAsByteArray();
    }

}
