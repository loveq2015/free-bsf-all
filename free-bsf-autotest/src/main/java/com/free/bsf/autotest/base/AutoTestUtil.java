package com.free.bsf.autotest.base;

import com.free.bsf.autotest.AutoTestContext;
import com.free.bsf.autotest.AutoTestProperties;
import com.free.bsf.autotest.base.wrapper.IRequestWrapper;
import com.free.bsf.autotest.base.wrapper.WrapperFactory;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.JsonUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.val;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.*;

public class AutoTestUtil {
    private static String parseHeadersToJson (HttpServletRequest request) {
        Map map = new HashMap();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String name  = headerNames.nextElement();
            String value = request.getHeader(name);
            map.put(name,value);
        }
        return JsonUtils.serialize(map);
    }

    private static String getBody(HttpServletRequest request){
        return AutoTestIOUtils.toString(((IRequestWrapper)request).getBody());
    }

    public static RequestInfo getRequestInfo(HttpServletRequest request){
        RequestInfo requestInfo = new RequestInfo();
        requestInfo.setBody(getBody(request));
        requestInfo.setHeader(parseHeadersToJson(request));
        requestInfo.setUrl(AutoTestUtil.getEntireUrl(request));
        requestInfo.setFromIp(PropertyUtils.getPropertyCache(CoreProperties.BsfNetworkIP,""));
        requestInfo.setAppName(PropertyUtils.getPropertyCache(CoreProperties.SpringApplicationName,""));
        requestInfo.createTime = new Date();
        requestInfo.method = request.getMethod();
        requestInfo.setOperatorType(AutoTestUtil.getOperatorType(request));
        requestInfo.setTraceTop(AutoTestUtil.isTraceTop(request)==true?"是":"否");
        val traceid =getTraceId(request);
        requestInfo.setTraceId(traceid==null?"":traceid);
        return requestInfo;
    }

    public static boolean hitSkipUrl(HttpServletRequest request){
        String skipUrlsStr = PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestRecordSkipUrls,"");
        if(StringUtils.isEmpty(skipUrlsStr))
            return false;
        String[] skipUrls = skipUrlsStr.split(",");
        String url = request.getRequestURI();
        String trimChar="*";
        for(val skip : skipUrls) {
            String trimUrl=StringUtils.trim(skip,'*');
            if(!skip.startsWith(trimChar)&&!skip.endsWith(trimChar)){
                if(url.equals(trimUrl))
                    return true;
            }
            else if(skip.startsWith(trimChar)&&skip.endsWith(trimChar)){
                if(url.contains(trimUrl))
                    return true;
            }else if(skip.startsWith(trimChar)){
                if(url.endsWith(trimUrl))
                    return true;
            }else if(skip.endsWith(trimChar)){
                if(url.startsWith(trimUrl))
                    return true;
            }
        }
        return false;
    }

    public static boolean isTestRunning(HttpServletRequest request){
        if(request.getHeader(AutoTestProperties.AutoTestRunning)!=null){
            return true;
        }
        return false;
    }

    public static boolean hitFrequency(HttpServletRequest request){
        //有traceId的,跳过频率限制
        if(!StringUtils.isEmpty(getTraceId(request)))
            return true;
        val recordCount = AutoTestContext.Context.getRecordCount().getAndIncrement();
        val frequency = PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestRecordFrequency, 1);
        if(frequency<=0){
            return false;
        }
        if (recordCount % frequency == 0) {
            return true;
        }
        return false;

    }

    public static String getEntireUrl(HttpServletRequest request){
        String baseUrl = request.getRequestURL().toString();
        if(!StringUtils.isEmpty(request.getQueryString())){
            return baseUrl+"?"+StringUtils.nullToEmpty(request.getQueryString());
        }
        return baseUrl;
    }

    public static boolean isTraceTop(HttpServletRequest request){
        Object traceId=request.getHeader(AutoTestProperties.AutoTestTraceId);
        if(traceId!=null)
            return false;
        else
            return true;
    }

    public static String getTraceId(HttpServletRequest request){
        Object traceId=request.getHeader(AutoTestProperties.AutoTestTraceId);
        if(traceId!=null)
            return (String)traceId;
        traceId =request.getAttribute(AutoTestProperties.AutoTestTraceId);
        if(traceId!=null)
            return  (String)(traceId);
        return null;
    }

    public static boolean hasReWriteRequest(HttpServletRequest request){
        if(request!=null && (WrapperFactory.isWrapper(request) || "true".equals(request.getAttribute(AutoTestProperties.AutoTestReWriteRequest))))
            return true;
        else
            return false;
    }

    public static void setTraceId(HttpServletRequest request){
        if(PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestTraceIdEnabled,true)) {
            val traceId = getTraceId(request);
            if (!StringUtils.isEmpty(traceId)) {
                request.setAttribute(AutoTestProperties.AutoTestTraceId, traceId);
            } else {
                val appName = PropertyUtils.getPropertyCache(CoreProperties.SpringApplicationName, "");
                val uuid = UUID.randomUUID().toString().replace("-", "");
                request.setAttribute(AutoTestProperties.AutoTestTraceId, appName + "-" + uuid);
            }
        }
    }

    public static String getOperatorType(HttpServletRequest request){
        if(request!=null) {
            Object queryOnly = request.getAttribute(AutoTestProperties.AutoTestOperatorType);
            if (queryOnly != null) {
                if ((queryOnly instanceof String) && OperatorTypeEnum.queryOnly.getName().equalsIgnoreCase((String) queryOnly))
                    return OperatorTypeEnum.queryOnly.getName();
                else
                    return OperatorTypeEnum.operator.getName();
            }
        }
        return OperatorTypeEnum.none.getName();
    }

    public static void setOperatorType(HttpServletRequest request,String operatorType){
        if(PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestOperatorTypeEnabled,true)) {
            val operatorType2 = getOperatorType(request);
            if (OperatorTypeEnum.operator.getName().equalsIgnoreCase(operatorType2)) {
                request.setAttribute(AutoTestProperties.AutoTestOperatorType, OperatorTypeEnum.operator.getName());
            } else {
                request.setAttribute(AutoTestProperties.AutoTestOperatorType, operatorType);
            }
        }
    }


}
