package com.free.bsf.shardingjdbc;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author: chejiangyi
 * @version: 2019-05-31 13:18
 **/
@Configuration
@ConditionalOnProperty(name = ShardingJdbcProperties.SpringShardingSphereEnabled, havingValue = "true")
public class ShardingJdbcConfiguration {
    @Bean
    @ConditionalOnProperty(name = "bsf.shardingjdbc.aspect.enabled", havingValue = "true")
    @ConditionalOnClass(name = "org.aspectj.lang.annotation.Aspect")
    public ShardingJdbcDynamicDataSourceAspect shardingJdbcDynamicDataSourceAspect(){
        return new ShardingJdbcDynamicDataSourceAspect();
    }
}
