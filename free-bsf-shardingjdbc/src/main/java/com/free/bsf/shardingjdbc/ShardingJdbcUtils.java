package com.free.bsf.shardingjdbc;

import com.free.bsf.core.base.Callable;
import org.apache.shardingsphere.infra.hint.HintManager;

/**
 * @author: chejiangyi
 * @version: 2019-09-01 14:16
 **/
public class ShardingJdbcUtils {
    public static <T> T hitMasterOnly(Callable.Func0<T> action){
        try(HintManager hintManager=HintManager.getInstance()){
            hintManager.setPrimaryRouteOnly();
            return action.invoke();
        }
    }

    public static <T> T hitSlaveOnly(Comparable<?> slave,Callable.Func0<T> action){
        return hitDataSource(slave,action);
    }

    public static <T> T hitDataSource(Comparable<?> name,Callable.Func0<T> action){
        try(HintManager hintManager=HintManager.getInstance()){
            hintManager.setDatabaseShardingValue(name);
            return action.invoke();
        }
    }
}
