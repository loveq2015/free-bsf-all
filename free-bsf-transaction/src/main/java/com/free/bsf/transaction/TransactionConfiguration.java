package com.free.bsf.transaction;

import com.free.bsf.core.config.BsfConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({BsfConfiguration.class,SeataConfiguration.class})
@ConditionalOnProperty(name = "bsf.transaction.enable", havingValue = "true")
public class TransactionConfiguration {
}
