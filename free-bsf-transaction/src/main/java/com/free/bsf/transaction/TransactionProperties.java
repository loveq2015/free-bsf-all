package com.free.bsf.transaction;

import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

public class TransactionProperties {
    public static boolean getEnable(){
        return PropertyUtils.getPropertyCache("bsf.transaction.enable",false);
    }
    public static boolean getSeataEnable(){
        return PropertyUtils.getPropertyCache("bsf.transaction.seata.enable",false);
    }
    public static String getSeataDataSourceProxyMode(){
        return PropertyUtils.getPropertyCache("bsf.transaction.seata.dataSourceProxyMode","at");
    }
    public static String getSeataTxServiceGroup(){
        return PropertyUtils.getPropertyCache("bsf.transaction.seata.txServiceGroup", CoreProperties.getApplicationName()+"-seata-service-group");
    }
    public static String[] getExcludes(){
        String excludes = PropertyUtils.getPropertyCache("bsf.transaction.seata.excludes", "");
        if(!StringUtils.isEmpty(excludes)){
            return excludes.split(",");
        }
        return new String[]{};
    }
    public static String Project="Transaction";
    public static String SpringApplicationName="spring.application.name";
    public static String SeataVgroupMapping="service.vgroupMapping.{txServiceGroup}";
}
