package com.free.bsf.elk;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.turbo.TurboFilter;
import com.free.bsf.core.common.Collector;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.AbstractMatcherFilter;
import ch.qos.logback.core.spi.FilterReply;
import com.free.bsf.core.util.LogUtils;
import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 错误日志统计拦截器
 * 
 * @author Robin.Wang
 * @date 2010-10-22
 */
public class LogStatisticsFilter extends TurboFilter {

	private static long lastCollectTime=System.currentTimeMillis()/60000;
	private static volatile AtomicLong errorCount= new AtomicLong(0);
	private static volatile AtomicLong logCount= new AtomicLong(0);
	public LogStatisticsFilter init(){
		ILoggerFactory factory = LoggerFactory.getILoggerFactory();
		if (factory instanceof LoggerContext) {
			LoggerContext context = ((LoggerContext) factory);
			context.addTurboFilter(this);
		}
		return this;
	}
	@Override
	public FilterReply decide(Marker marker, Logger logger, Level level, String format,
							  Object[] params, Throwable t) {
		if (level.equals(Level.ERROR)) {
			errorCount.incrementAndGet();
		}
		if(level.equals(Level.ERROR)||level.equals(Level.WARN)||level.equals(Level.INFO)){
			logCount.incrementAndGet();
		}
		if(System.currentTimeMillis()/60000>lastCollectTime)
		{
			lastCollectTime=System.currentTimeMillis()/60000;
			logCount.set(0);
			errorCount.set(0);
		}
		Collector.Default.value("bsf.health.log.error.count").set(errorCount);
		Collector.Default.value("bsf.health.log.incre.count").set(logCount);
		return FilterReply.NEUTRAL;
	}
}
