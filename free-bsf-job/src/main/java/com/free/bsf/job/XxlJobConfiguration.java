package com.free.bsf.job;

import com.free.bsf.core.config.CoreProperties;
import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import com.free.bsf.core.config.BsfConfiguration;
import com.free.bsf.core.base.BsfEnvironmentEnum;
import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.NetworkUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Huang Zhaoping
 */
@Configuration
@ConditionalOnProperty(name = "bsf.job.enabled", havingValue = "true")
@Import(BsfConfiguration.class)
public class XxlJobConfiguration {

	@Bean(initMethod = "start", destroyMethod = "destroy")
	public XxlJobSpringExecutor xxlJobExecutor() {
		String appName = XxlProperties.getAppName().length() == 0 ? CoreProperties.getApplicationName()
				: XxlProperties.getAppName();
		if (appName.length() == 0) {
			throw new BsfException("缺少参数：xxl.job.executor.appname");
		}
		var adminAddresses = XxlProperties.getAdminAddresses();
		if(StringUtils.isEmpty(adminAddresses))
			throw new BsfException("缺少xxl.job.admin.addresses 配置");
		XxlJobSpringExecutor executor = new XxlJobSpringExecutor();
		executor.setAdminAddresses(adminAddresses);
		executor.setAppName(appName);
		executor.setIp(XxlProperties.getIp());
		if(StringUtils.isEmpty(XxlProperties.getIp())) {
			executor.setIp(NetworkUtils.getIpAddress());
		}	
		executor.setPort(XxlProperties.getPort());
		executor.setAccessToken(XxlProperties.getAccessToken());
		executor.setLogPath(XxlProperties.getLogPath());
		executor.setLogRetentionDays(XxlProperties.getLogRetentionDays());
		LogUtils.info(XxlJobConfiguration.class, "Job","已启动!!! " + XxlProperties.XxlJobAdminAddresses + "=" + adminAddresses);
		return executor;
	}
	

}
