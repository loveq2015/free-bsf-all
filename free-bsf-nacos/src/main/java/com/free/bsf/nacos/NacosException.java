package com.free.bsf.nacos;

import com.free.bsf.core.base.BsfException;

public class NacosException extends BsfException {
    public NacosException(String message,Exception exp){
        super(message, exp);
    }
    public NacosException(String message){
        super(message);
    }
    public NacosException(Exception exp){
        super(exp);
    }
}
