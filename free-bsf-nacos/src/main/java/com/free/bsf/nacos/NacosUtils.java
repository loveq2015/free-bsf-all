package com.free.bsf.nacos;

import com.alibaba.nacos.api.naming.NamingService;
import com.free.bsf.core.util.ContextUtils;
import lombok.val;

public class NacosUtils {
    public static NamingService getNamingService(){
        val config = ContextUtils.tryGetBean(NacosRegistry.class);
        if(config!=null){
            return config.getNamingService();
        }
        return null;
    }
}
