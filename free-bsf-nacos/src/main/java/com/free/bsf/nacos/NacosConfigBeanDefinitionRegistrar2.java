package com.free.bsf.nacos;

import com.alibaba.nacos.spring.context.annotation.config.EnableNacosConfig;
import com.alibaba.nacos.spring.context.annotation.config.NacosConfigBeanDefinitionRegistrar;
import com.alibaba.nacos.spring.util.NacosBeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;

public class NacosConfigBeanDefinitionRegistrar2 extends NacosConfigBeanDefinitionRegistrar {
    private Environment environment;
    private BeanFactory beanFactory;

    public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {
        if(NacosProperties.configEnabled()) {
            AnnotationAttributes attributes = NacosProperties.getEnableNacosConfig();
            NacosBeanUtils.registerGlobalNacosProperties((Map)attributes, registry, this.environment, "globalNacosProperties$config");
            NacosBeanUtils.registerNacosCommonBeans(registry);
            NacosBeanUtils.registerNacosConfigBeans(registry, this.environment, this.beanFactory);
            NacosBeanUtils.invokeNacosPropertySourcePostProcessor(this.beanFactory);
        }
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
}
