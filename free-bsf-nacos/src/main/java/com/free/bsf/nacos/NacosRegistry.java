package com.free.bsf.nacos;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.naming.NamingService;
import com.free.bsf.api.apiregistry.INacosRegistry;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.ConvertUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.core.util.WebUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.val;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NacosRegistry implements INacosRegistry {
    @NacosInjected
    @Getter
    @Setter
    private NamingService namingService;

    String ip;
    int port;
    public NacosRegistry(){
    }

    public NamingService getNamingService(){
        if(namingService==null)
            throw new NacosException("nacos NameService为空");
        return namingService;
    }

    @Override
    public void register(){
        try {
            if(StringUtils.isEmpty(WebUtils.getHost())){
                throw new NacosException("本地host为空");
            }
            ip = WebUtils.getHost().split(":")[0];
            port = ConvertUtils.convert( WebUtils.getHost().split(":")[1],int.class);
            getNamingService().registerInstance(CoreProperties.getApplicationName(), ip, port);
        }catch (Exception e){
            throw new NacosException(e);
        }
    }

    @Override
    public Map<String, List<String>> getServerList(){
        Map<String, List<String>> rs = new HashMap<>();
        try {
            for (val service : getNamingService().getServicesOfServer(0,1000000).getData()) {
                for (val s : getNamingService().selectInstances(service, true)) {
                    val servers = new ArrayList<String>();
                    servers.add(s.getIp() + ":" + s.getPort());
                    rs.put(getAppNameFromKey(s.getServiceName()), servers);
                }
            }
        }catch (Exception e){
            throw new NacosException(e);
        }
        return rs;
    }

    /**从key中解析获取appName*/
    private String getAppNameFromKey(String key) {
        val infos = key.split("@");
        return infos[2];
    }

    @Override
    public void close(){
        try {
            getNamingService().deregisterInstance(CoreProperties.getApplicationName(), ip, port);
        }catch (Exception e){
            throw new NacosException(e);
        }
    }
}
