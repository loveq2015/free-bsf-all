package com.free.bsf.nacos;


import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.PropertyUtils;
import lombok.val;
import org.springframework.core.annotation.AnnotationAttributes;

import java.util.HashMap;
import java.util.Map;

public class NacosProperties {
    public final static String Project = "Nacos";
    public final static String BsfNacosEnabled="bsf.nacos.enabled";
    public final static String BsfNacosDiscoveryEnabled="bsf.nacos.discovery.enabled";
    public final static boolean enabled() {
        return PropertyUtils.getPropertyCache("bsf.nacos.enabled", false);
    }
    public final static boolean configEnabled() {
        return PropertyUtils.getPropertyCache("bsf.nacos.config.enabled", false);
    }
    public final static boolean discoveryEnabled() {
        return PropertyUtils.getPropertyCache("bsf.nacos.discovery.enabled", false);
    }
    public final static String endpoint() {
        return PropertyUtils.getPropertyCache("nacos.endpoint", "");
    }

    public final static String namespace() {
        return PropertyUtils.getPropertyCache("nacos.namespace", "");
    }

    public final static String accessKey() {
        return PropertyUtils.getPropertyCache("nacos.access-key", "");
    }

    public final static String secretKey() {
        return PropertyUtils.getPropertyCache("nacos.secret-key", "");
    }

    public final static String serverAddr() {
        return PropertyUtils.getPropertyCache("nacos.server-addr", "");
    }

    public final static String contextPath() {
        return PropertyUtils.getPropertyCache("nacos.context-path", "");
    }

    public final static String clusterName() {
        return PropertyUtils.getPropertyCache("nacos.cluster-name", "");
    }

    public final static String encode() {
        return PropertyUtils.getPropertyCache("nacos.encode", "");
    }

    public final static String configLongPollTimeout() {
        return PropertyUtils.getPropertyCache("nacos.configLongPollTimeout", "");
    }

    public final static String configRetryTime() {
        return PropertyUtils.getPropertyCache("nacos.configRetryTime", "");
    }

    public final static String maxRetry() {
        return PropertyUtils.getPropertyCache("nacos.maxRetry", "");
    }

    public final static String enableRemoteSyncConfig() {
        return PropertyUtils.getPropertyCache("nacos.enableRemoteSyncConfig", "");
    }

    public final static String username() {
        return PropertyUtils.getPropertyCache("nacos.username", "");
    }

    public final static String password() {
        return PropertyUtils.getPropertyCache("nacos.password", "");
    }

    public final static AnnotationAttributes getEnableNacosConfig(){
        NacosProperties.initDefaultProperty();
        val map = new HashMap<String,Object>();
        map.put("endpoint",endpoint());
        map.put("namespace",namespace());
        map.put("accessKey",accessKey());
        map.put("secretKey",secretKey());
        map.put("serverAddr",serverAddr());
        map.put("contextPath",contextPath());
        map.put("clusterName",clusterName());
        map.put("encode",encode());
        map.put("configLongPollTimeout",configLongPollTimeout());
        map.put("configRetryTime",configRetryTime());
        map.put("maxRetry",maxRetry());
        map.put("enableRemoteSyncConfig",enableRemoteSyncConfig());
        map.put("username",username());
        map.put("password",password());
        return new AnnotationAttributes(map);

    }

    public final static AnnotationAttributes getEnableNacosDiscovery(){
        initDefaultProperty();
        val map = new HashMap<String,Object>();
        map.put("endpoint",endpoint());
        map.put("namespace",namespace());
        map.put("accessKey",accessKey());
        map.put("secretKey",secretKey());
        map.put("serverAddr",serverAddr());
        map.put("contextPath",contextPath());
        map.put("clusterName",clusterName());
        map.put("username",username());
        map.put("password",password());
        return new AnnotationAttributes(map);
    }

    private static void initDefaultProperty(){
        PropertyUtils.setDefaultInitProperty(NacosProperties.class,Project,"nacos.namespace",CoreProperties.getEnv().name());
    }
}
