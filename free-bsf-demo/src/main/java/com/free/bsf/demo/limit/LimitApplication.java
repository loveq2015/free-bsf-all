package com.free.bsf.demo.limit;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.free.bsf.core.http.DefaultHttpClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@SpringBootApplication
public class LimitApplication {
    public static void main(String[] args) {
        SpringApplication.run(LimitApplication.class, args);
        ExecutorService executor = Executors.newFixedThreadPool(1);
        while (true) {
            executor.submit(() -> {
                log.info(DefaultHttpClient.Default.get("http://localhost:8081/hello"));
                
            });
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    @RequestMapping("/hello4")
    public Object hello4(String name) {
        return new Date();
    }

    @GetMapping("/hello3")
    public Object hello3(String name) {
        try {
            Thread.sleep(60 * 1000);
        } catch (Exception e) {
        }
        return "60s ok";
    }

}
