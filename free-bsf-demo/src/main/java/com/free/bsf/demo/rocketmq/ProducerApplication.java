package com.free.bsf.demo.rocketmq;


import com.free.bsf.core.serialize.JsonSerializer;
import com.free.bsf.mq.base.DelayTimeEnum;
import com.free.bsf.mq.rocketmq.RocketMQSendMessage;
import com.free.bsf.mq.rocketmq.RocketMQSubscribeRunable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.free.bsf.mq.rocketmq.RocketMQConsumerProvider;
import com.free.bsf.mq.rocketmq.RocketMQProducerProvider;

@SpringBootApplication
@RestController //implements WebMvcConfigurer
public class ProducerApplication  {

    @Autowired(required = false)
    private RocketMQProducerProvider rocketMQProducerProvider;
//    @Autowired(required = false)
//    private RocketMQConsumerProvider rocketMQConsumerProvider;

    public static void main(String[] args){
        ApplicationContext context = SpringApplication.run(ProducerApplication.class, args);
        RocketMQConsumerProvider consumerProvider = context.getBean(RocketMQConsumerProvider.class);
        consumerProvider.subscribe(new RocketMQSubscribeRunable<String>()
                .setConsumerGroup("GID_free-bsf-demo-test-consumer-01")
                .setFilterTags(null)
                .setType(String.class)
                .setRunnable((msg)->{
                    System.out.println(new JsonSerializer().serialize( msg));
                }));

        consumerProvider.subscribe(
                "GID_free-bsf-demo-test-consumer-01","free-bsf-demo-test",null,(msg)->{
            System.out.println(new JsonSerializer().serialize( msg));
        },String.class);
    }
    int i=0;
    @GetMapping("/sendMessage")
    public void sendMessage() {
        i=i+1;
        rocketMQProducerProvider.sendMessage(new RocketMQSendMessage<String>()
                .setTag("ddd")
                .setQueueName("free-bsf-demo-test")
                .setMsg("aaaa")
        );

    }
    @GetMapping("/sendMessage2")
    public void sendMessage2(int partition) {
        i=i+1;
        rocketMQProducerProvider.sendMessage("free-bsf-demo-test","aaaa","测试"+ i, DelayTimeEnum.H01,(queueSize)->{
            return partition;
        });

    }
}