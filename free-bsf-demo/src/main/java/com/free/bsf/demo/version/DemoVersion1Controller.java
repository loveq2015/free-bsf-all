package com.free.bsf.demo.version;

import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;




/**
 * RPC 版本demo version 1
 * Robin.Wang
 * */
@RestController
public class DemoVersion1Controller {

    
    @GetMapping("/hello")
    public Object hello(String name) {
        return "hello version 1.0 "+ new Date();
    }
}
