package com.free.bsf.demo.nacos;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NacosConfig {
     @NacosValue(value = "${useLocalCache:''}", autoRefreshed = true)
     @Getter @Setter
     private String useLocalCache;
     @NacosValue(value = "${a:''}", autoRefreshed = true)
     @Getter @Setter
     private String a;
//    @Bean("data1")
//    @ConfigurationProperties("spring.datasource.data1")
//    @Primary
//    public DataSource data1DataSource(){
//        return DataSourceBuilder.create().type(ReflectionUtils.classForName(PropertyUtils.getPropertyCache("spring.datasource.data1.type",""))).build();
//    }
//    @Bean("data2")
//    @ConfigurationProperties("spring.datasource.data2")
//    public DataSource data2DataSource(){
//        return DataSourceBuilder.create().type(ReflectionUtils.classForName(PropertyUtils.getPropertyCache("spring.datasource.data2.type",""))).build();
//    }

}
