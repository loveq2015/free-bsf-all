package com.free.bsf.demo.es;

import com.free.bsf.elasticsearch.impl.ElasticSearchDruidSqlProvider;
import com.free.bsf.elasticsearch.impl.ElasticSearchRestSqlProvider;
import com.google.common.collect.Lists;
import com.free.bsf.elasticsearch.base.Param;
import com.free.bsf.elasticsearch.impl.ElasticSearchSqlProvider;
import com.free.bsf.elk.requestid.RequestUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@SpringBootApplication //implements WebMvcConfigurer
public class DemoApplication  {

	@Autowired
	private ElasticSearchRestSqlProvider searchService;

    @GetMapping("/selectByIndex")
    public Object selectByIndex() {
//    	Integer result = searchService.searchObjectBySql("select count(*) as num from aaa",Param.create(5), Integer.class);
//    	return result;

		List<HashMap> result = searchService.searchListBySql("SELECT sum(isTask) as num,isTask,MIN(isTask) FROM aaa group by taskName",Param.create(), HashMap.class);
		return result;
    }

    //  SELECT * FROM indexName/type
    @GetMapping("/selectByType")
    public Object selectByType() {
		List<AAA> result = searchService.searchListBySql("SELECT * FROM aaa limit 0,{}",Param.create(5),AAA.class);
    	return result;
    }

	@GetMapping("/selectListBySql")
	public Object selectListBySql(String sql) {
		return searchService.searchListBySql(sql,Param.create(),Object.class);
	}

	@GetMapping("/searchObjectBySql")
	public Object searchObjectBySql(String sql) {
		return searchService.searchObjectBySql(sql,Param.create(),Object.class);
	}


	@GetMapping("/insert")
    public Object insert() {
		if(!searchService.indexExist("aaa")){
			searchService.createIndex("aaa",AAA.class);
		}
    	List<AAA> result = Lists.newArrayList();
    	result.add(new AAA("-1", "美女",false));
    	result.add(new AAA("5", "中国人",true));
    	result.add(new AAA("333", "中国美女",false));
    	searchService.insertData("aaa", result);
    	return result;
    }
	@GetMapping("/deleteBySql")
	public boolean deleteBySql(String sql) {
    	searchService.deleteBySql(sql);
    	return true;
	}
    @GetMapping("/delete")
    public Object delete() {
    	try {
			searchService.deleteBySql("delete from aaa where messageId = {}", Param.create(5));
			return "";
		}catch (Exception ex){
    		return null;
		}
    }
    
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		//MDC.put(RequestUtil.REQUEST_TRACEID, RequestUtil.getRequestId());
	}

	@Document(indexName = "aaa")
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class AAA {
		@Id
		@Field(type = FieldType.Keyword, store = true,fielddata=true)
		String messageId;//消息唯一标识
		@Field(type = FieldType.Text, analyzer = "ik_smart", index = true, store = true,fielddata=true)
		String taskName;//批量任务名
		@Field(store = true,type = FieldType.Boolean)
		Boolean isTask;//是否是批量任务
	}

	@Document(indexName = "notify_index")
	@Data
	public static class EsNotify {
		@Id
		@Field(type = FieldType.Keyword, store = true)
		String messageId;//消息唯一标识
		@Field(type = FieldType.Text, analyzer = "ik_smart", index = true, store = true)
		String taskName;//批量任务名
		@Field(store = true)
		Boolean isTask;//是否是批量任务
		@Field(type = FieldType.Keyword, store = true)
		String templateKey;//消息模板key
		@Field(type = FieldType.Text, analyzer = "ik_smart", index = true, store = true)
		String messageParams;//消息体
		@Field(type = FieldType.Keyword, store = true)
		String receiveUser;//消息接收人
		@Field(type = FieldType.Integer, store = true)
		Integer retryCount;//重试次数
		@Field(type = FieldType.Keyword, store = true)
		String retryTimespan;//重试间隔
		@Field(type = FieldType.Integer, store = true)
		Integer failTotal;//错误记录数
		@Field(type = FieldType.Integer, store = true)
		Integer sendState;//发送状态
		@Field(type = FieldType.Integer, store = true)
		Integer receiveState;//接收状态
		@Field(type = FieldType.Keyword, store = true)
		String receiveStateDesc;//接收状态描述 add by clz.xu 2021年5月7日16:10:01
		@Field(type = FieldType.Integer, store = true)
		Integer effectiveTime;//有效时间
		@Field(type = FieldType.Keyword, store = true)
		String sendResult;//发送结果信息
		@Field(store = true)
		Date createTime;//创建时间
		@Field(store = true)
		Date updateTime;//更新时间
		@Field(type = FieldType.Keyword,store = true)
		String createUser;//创建人
		@Field(type = FieldType.Keyword,store = true)
		String updateUser;//更新人
		@Field(type = FieldType.Integer, store = true)
		Integer partition;//消息通道
		@Field(type = FieldType.Keyword,store = true)
		String bizId;//第三方回执Id  add by clz.xu 2021年5月7日16:10:01
	}

}
