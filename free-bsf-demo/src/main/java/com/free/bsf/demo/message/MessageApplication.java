package com.free.bsf.demo.message;


import com.free.bsf.core.util.WarnUtils;
import com.free.bsf.message.channel.AbstractMessageProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
@Slf4j
public class MessageApplication {
    @Autowired(required =false)
    private AbstractMessageProvider messageProvider;

    public static void main(String[] args){
        SpringApplication.run(MessageApplication.class, args);
        log.error("【报警】测试");
        log.error("[报警]测试");
    }
    @GetMapping("/message/{content}")
    public void sendMessage(@PathVariable  String content) throws Exception {
        messageProvider.sendText(content);

    }

    @GetMapping("/notifynow/{content}")
    public void notifynow(@PathVariable String content) throws Exception {
        WarnUtils.notifynow("ERROR", "测试", content);
    }


}