package com.free.bsf.demo.nacos;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.annotation.NacosProperties;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.spring.context.annotation.config.EnableNacosConfig;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import com.alibaba.nacos.spring.context.annotation.discovery.EnableNacosDiscovery;
import com.free.bsf.core.db.DbHelper;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.ThreadUtils;
import com.free.bsf.nacos.NacosUtils;
import lombok.val;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.sql.DataSource;

@SpringBootApplication
//@EnableNacosDiscovery(globalProperties = @NacosProperties(serverAddr = "127.0.0.1:8848"))
public class NacosApplication {


    public static void main(String[] args) {
        SpringApplication.run(NacosApplication.class, args);
        for(int i=0;i<1000;i++) {
            val service= NacosUtils.getNamingService();
            if(service!=null) {
            }
            String str = ContextUtils.getBean(NacosConfig.class, false).getUseLocalCache();
           System.out.println("value:"+str);
            String a = ContextUtils.getBean(NacosConfig.class, false).getA();
            System.out.println("value-a:"+a);
            System.out.println("线程池:"+ThreadUtils.system().isShutdown());
           ;
            ThreadUtils.sleep(1000*5);
        }
    }
}
