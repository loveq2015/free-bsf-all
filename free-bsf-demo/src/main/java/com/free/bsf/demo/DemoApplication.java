package com.free.bsf.demo;

import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.ConvertUtils;
import com.free.bsf.core.util.DateUtils;
import com.free.bsf.core.util.ThreadUtils;
import com.free.bsf.demo.job.HelloJob;
import com.free.bsf.redis.RedisProvider;
import com.free.bsf.redis.limit.RedisLimitFactory;
import com.free.bsf.redis.limit.RedisLimitType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.lang.ref.ReferenceQueue;
import java.util.*;

//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@RestController
@Api("客户管理服务")
@SpringBootApplication
public class DemoApplication implements AbstractDemo //implements WebMvcConfigurer
         {
public static String key(String className,String methodName){
    return "ok";
}

    //@GetMapping("/hello")
    @ApiOperation("rrr")
    public Object hello444(String name) {
        return new Date();
    }

    // @GetMapping("/hello2")
     @ApiOperation("333")
     public Object hello2(String name) {
       try{ Thread.sleep(60*1000);}catch (Exception e){}
        return "60s ok";
     }

    @Bean
    public HelloJob helloJob(){
        return new HelloJob();
    }

    @GetMapping("/logger")
    @ApiOperation("111")
    public String logger(String msg){
        log.info(msg);
        return "success";
    }


    @Data
    private static class Test{
        String msg= UUID.randomUUID().toString();
        Integer index=null;
       // byte[] data = new byte[1024*1024];

//        @Override
//        protected void finalize() throws Throwable {
//            super.finalize();
//        }

    }

    private static ReferenceQueue<Test> QUEUE = new ReferenceQueue<Test>();
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        val redis = ContextUtils.getBean(RedisProvider.class,false);
//        val jRedis = new Jedis("r-bp1i0ner52jw4b5btt.redis.rds.aliyuncs.com",6379);
//        val redis = RedisLimitFactory.create(jRedis,RedisLimitType.Slide);
        int i=0;
        while (true) {
            i++;
            if (redis.limit("测试限流", 1, 10,RedisLimitType.Count)) {
                System.out.println(i+"访问"+ DateUtils.format(new Date(),"HH:mm:ss:SSS"));
                ThreadUtils.sleep(10);
            } else {
                System.err.println(i+"已限流"+ DateUtils.format(new Date(),"HH:mm:ss:SSS"));
                ThreadUtils.sleep(10);
            }
        }
//        ReferenceQueue QUEUE2 = new ReferenceQueue();
//
//        try {
//            //List<Test> as = new ArrayList<>();
//            List<Reference> rs = new ArrayList<>();
//            for(int i=0;i<100;i++)
//             {
//                Test test = new Test();
//                test.setIndex(i);
//                 PhantomReference test2 = new PhantomReference(test, QUEUE2);
//                //test=null;
//                //as.add(test);
//                 rs.add(test2);
//            }
////            Test test = new Test();
////            WeakReference test2 = new WeakReference(test, QUEUE2);
////            for(Object a:as) {
////
////            }
////            for(Object a:as) {
////                a=null;
////            }
//            //System.out.println(test2.isEnqueued());
//            //System.out.println(QUEUE2.remove(1000));
//            //System.out.println(test2.isEnqueued());
//            //as.clear();
////            test=null;
////            test2=null;
//            System.gc();;
//            System.gc();;
//            System.gc();;
//        }catch (Exception e){
//            System.err.println(e);
//        }
//
//        int a=1;
//        try {
//            var data = QUEUE2.remove(2000);
//            System.out.println(data);
//        }catch (Exception e){
//            System.err.println(e);
//        }
//        var t2 = new Thread(()-> {
//            while (true) {
//                Test t = new Test();
//                PhantomReference obj = new PhantomReference<>(t, QUEUE);
//                //WeakReference obj = new WeakReference<>(t, QUEUE);
//                t=null;
//                ThreadUtils.sleep(1000);
//
//            }
//        });
//        t2.setDaemon(true);
//        t2.start();
//
//        var t1= new Thread(()->{
//            while (true) {
//                try {
//                    var r = QUEUE.remove(1000);
//                    if (r != null) {
//                        System.out.println(r);
//                    }
//                }catch (Exception e){
//                    System.out.println(e.getMessage());
//                }
//            }
//        });
//        t1.setDaemon(true);
//        t1.start();
//
//        while (true) {
//            ThreadUtils.sleep(5000);
//            System.gc();
//            System.out.println("GC");
//        }
        //System.out.println("aaadfadf");
        //URLClassLoader
        //JarLauncher
//        SpringApplicationBuilder builder = new SpringApplicationBuilder(DemoApplication.class);
//        SpringApplication application = builder.application();
//        application.addListeners((l)->{
//            System.out.println(l.getSource().getClass());
//        });
//        val context = application.run(args);
        //CompressUtils.zip(new File("catlogs").getAbsolutePath(),new File("catlogs.zip").getAbsolutePath());
        //CompressUtils.unzip(new File("catlogs.zip").getAbsolutePath(),new File("catlogs-bin").getAbsolutePath());
       // System.out.println("dfasdfasdlfjlajdsflOKOK");
        //BeanDefinitionRegistry registry, AnnotationMetadata annotationMetadata, Map<String, Object> attributes
        //ApplicationResource
        //EurekaServiceRegistry
        //ApplicationResource
        //var c = EurekaServerContextHolder.getInstance().getServerContext();
        //try{ Thread.sleep(30000);}catch (Exception e){}
        //System.exit(0);

//
//        val start0 = system.currentTimeMillis();
//        List<Integer> as = new ArrayList<>();
//        for(int i=1000;i>0;i--){
//            as.add(i);
//        }
//        system.out.println("for耗时:"+(system.currentTimeMillis()-start0));
//        //预热
//        ThreadPool.system.parallelFor2("测试",100,as,c->{
//            try{ Thread.sleep(c%30);}catch (Exception e){}
//        });
//        //try{ Thread.sleep(5000);}catch (Exception e){}
//
//
//        //算法2
//        List<Integer> result2 = Collections.synchronizedList(new ArrayList<>());
//        val start2 = system.currentTimeMillis();
//        ThreadPool.system.parallelFor2("测试2",10,as,c->{
//            //try{ Thread.sleep(c%30);}catch (Exception e){}
//            result2.add(c);
////            if(c%5==0||c==60){
////                throw new RuntimeException("aaa"+c);
////            }
//        });
//
//        system.out.println("算法2耗时:"+(system.currentTimeMillis()-start2));
//        val resuts2 = result2.toArray();
//        Arrays.sort(resuts2);
//        Arrays.stream(resuts2).forEach(c->{
//            system.out.println("算法2结果:"+c);
//        });

//        //算法1
//        List<Integer> result1 = Collections.synchronizedList(new ArrayList<>());
//        val start = system.currentTimeMillis();
//        ThreadPool.system.parallelFor("测试1",10,as,c->{
//            //try{ Thread.sleep(c%30);}catch (Exception e){}
//            result1.add(c);
//        });
//
//        system.out.println("算法1耗时:"+(system.currentTimeMillis()-start));
//        val resuts1 = result1.toArray();
//        Arrays.sort(resuts1);
//        Arrays.stream(resuts1).forEach(c->{
//            system.out.println("算法1结果:"+c);
//        });


//        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
//        system.out.println( new Date().toString());
//        system.out.println( new Date(new Date().toString()).toString());
       // SpringApplication.run(DemoApplication.class, args);
       //val item =  ContextUtils.getApplicationContext().getBean(HandlerExecutionChain.class);
       // WebUtils.getRequest().getServletContext().
        //InitialContext..doLookup("java:comp/env/jdbc");
        //val ds = ContextUtils.getApplicationContext().getBean("dataSource");
//        val a =((TomcatWebServer)((AnnotationConfigServletWebServerApplicationContext) ContextUtils.getApplicationContext()).getWebServer()).getTomcat().getServer().getUtilityExecutor();
//        ScheduledThreadPoolExecutor c=null;
        //var b= 1;
//        for( int i=0;i<10;i++) {
//            val b=i;
//            new Thread(() -> {
//                while (true) {
//                    try {
//                        Thread.sleep(5000 * b);
//                    } catch (Exception ex) {
//                    }
//                    throw new RuntimeException("aaa"+b);
//                }
//            }).start();
//        }
    }

}
