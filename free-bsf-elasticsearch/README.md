# ElasticSearch集成

### 介绍
ElasticSearch是一个基于Lucene的搜索服务器。它提供了一个分布式多用户能力的全文搜索引擎，基于RESTFUL web接口。
Elasticsearch是用Java语言开发的，并作为Apache许可条款下的开放源码发布，是一种流行的企业级搜索引擎。
[更多详情](https://www.elastic.co/cn/products/elasticsearch)
该组件集成用于统一封装ES的客户端，简化业务使用。
此版本核心采用spring-data-elasticsearch实现[spring-data-elasticsearch与elasticsearch版本对应](https://github.com/spring-projects/spring-data-elasticsearch/blob/main/src/main/asciidoc/preface.adoc#elasticsearch.operations)
同时采用elasticsearch-sql简化日常es的使用。
### 依赖引入
一般包已经通过free-bsf-starter依赖在脚手架项目中,无需额外配置
``` 
<dependency>
	<artifactId>free-bsf-elasticsearch</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7.1-SNAPSHOT</version>
</dependency>
```

### 配置说明
一般连接ElasticSearch配置参考
```
### 配置ES服务地址（使用原生的方式连接ES,druid sql支持）
bsf.elasticsearch.server = 127.0.0.1
###  ES批提交最小数量, 默认5
bsf.elasticsearch.bulkSize = 5
### 跳过ElasticsearchDataAutoConfiguration等springboot自动装配过滤,用于未使用es时优化启动性能
bsf.elasticsearch.autoConfigurationImportFilter.enabled=true
```
### es 操作封装
* [es原生操作支持](doc/README-common.md) [高阶版]
* [es-sql-druid模式支持](doc/README-druid.md) [已废弃,因为druid版本问题,无法使用]
* [es-sql-rest模式支持](doc/README-rest.md) [推荐使用]

### sql 参考语法
[es-sql语法参考,调试可视化参考](doc/README-sql.md)

### 部署参考
```
# 注意版本
-安装elasticsearch 
-安装head 插件
-安装elasticsearch-sql
-安装ik插件
```  
参考文档[es集群安装.pdf](doc/es集群安装.pdf),注意不同版本,安装细节不一样。
