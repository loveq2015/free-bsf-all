# ElasticSearch rest模式支持

### 介绍
ElasticSearch 采用rest sql模式进行连接,相比druid模式会使用更少的包。内部实现使用bsf http client的http连接池模式(相关组件优化依然有效),性能会有一定有优化。
假设未来es 采用第三方云服务,兼容起来更容易。

## 配置说明
连接ElasticSearch配置,注意一旦配置了server地址,底层的sql实现将会使用rest模式。
rest模式未来会优先支持，因为druid模式有druid版本问题，而且会引用太多不需要的包。
```
### 配置ES服务地址多个逗号隔开（使用原生的方式连接ES）
bsf.elasticsearch.server = 127.0.0.1
###  ES批提交最小数量, 默认5
bsf.elasticsearch.bulkSize = 5
```
## 代码示例
[es-sql语法参考,调试可视化参考](README-sql.md)
[索引相关操作,请看原生操作](README-common.md)
* 增删改查-使用ElasticSearchSqlProvider（查询推荐使用该组件-支持原生SQL语法）
```    
    @Autowired
    private ElasticSearchRestSqlProvider searchProvider;
    
    /*
     * 批量保存数据
     */
    public void insert() {
        List<OrderVo> orderList = new ArrayList<OrderVo>();
        
        OrderVo order = new OrderVo();
        order.setOrderId(1000001);
        order.setOrderNo("ON1000001");
        order.setProductName("苹果水果新鲜当季整箱陕西红富士应季");
        order.setCreateBy("刘某某");
        order.setAmount(new BigDecimal(1024.00));
        order.setOrderPostion(new GeoPoint(30.0f, 20.0f));
        order.setOrderDesc("哈哈我也不知道要写什么苹果,反正先来个50斤吧");
        orderList.add(order);
        
        order = new OrderVo();
        order.setOrderId(1000002);
        order.setOrderNo("ON1000002");
        order.setProductName("四川丑橘新鲜水果当季整箱10斤大果丑八怪橘子丑桔丑柑耙耙柑包邮");
        order.setCreateBy("车某某");
        order.setAmount(new BigDecimal(888.88));
        order.setOrderPostion(new GeoPoint(70.0f, 60.0f));
        order.setOrderDesc("要新鲜的甜的丑橘，不要发错了！！！！");
        orderList.add(order);
        
        searchProvider.insertData("orderindex", "ordertype", orderList);
    }
    
   /**
     * 模糊搜索商品名称匹配苹果的数据,返回的数据按相关性得分从高到低排序并且取前3条数据
     * 更多的sql语法请参考官方示例 https://github.com/NLPchina/elasticsearch-sql
     * Elasticsearch-sql官方提供了一个用于测试sql的网页客户端,非常方便测试sql的写法
     */
    public List<OrderVo> search() {
        List<OrderVo> orderList = provider.searchBySql("select * from orderindex where productName = matchQuery('苹果') ORDER BY _score DESC LIMIT 3", OrderVo.class);
        return orderList;
    }
    
   /**
     * 普通查询写法 
     */
    public List<OrderVo> selectByIndex() {
        List<OrderVo> orderList = provider.searchBySql("select * from orderindex where createBy = '车某某' and amount > 500 LIMIT 3", OrderVo.class);
        return orderList;
    }
    
   /**
     *  查询type写法
     *  SELECT * FROM indexName/type
     */
    public List<OrderVo> selectByType() {
        List<OrderVo> orderList = provider.searchBySql("select * from orderindex where createBy = '刘某某' and amount > 500 LIMIT 3", OrderVo.class);
        return orderList;
    }
    
    /**
     * 删除写法
     */
    public void delete() {
        provider.deleteBySql("delete from orderindex where orderId = 1000001");
    }
  ```

### 性能压测报告
暂无
