# ElasticSearch 原生模式支持

### 介绍
ElasticSearch 采用原生模式进行连接,支持es原生的一些es操作封装,方便熟悉原生api操作的人进行开发。

## 配置说明
连接ElasticSearch配置
```
### 配置ES服务地址（使用原生的方式连接ES）
bsf.elasticsearch.sever = 127.0.0.1
###  ES批提交最小数量, 默认5
bsf.elasticsearch.bulkSize = 5
```
## 代码示例
核心采用spring-data-elasticsearch的注解驱动为主。

```

@Document
public class OrderVo {
    
    @id
    @Field(type = FieldType.Integer)
    private Integer orderId;
    
    @Field(type = FieldType.Keyword)
    private String orderNo;
    
    @StringField(type = StringType.Text, analyzer = "ik_smart", index = true, store = false)
    private String productName;
    
    /**
     * 下单人 没注解也会自动映射
     */
    private String createBy;
    
    @Field(type = FieldType.Double)
    private BigDecimal amount;
    
    /*
     * 单字段多映射
     */
    @MultiField(
            mainField = @StringField(type = StringType.Keyword, boost = 2.0f),
            fields = {
                    @MultiNestedField(name = "cn", field = @StringField(type = StringType.Text, analyzer = "ik_smart")),
                    @MultiNestedField(name = "en", field = @StringField(type = StringType.Text, analyzer = "english")),
            },
            tokenFields = {
                    @TokenCountField(name = "cnTokenCount", analyzer = "ik_smart")
            }
    )
    private String orderDesc;
    
    /... getter and setter .../
 }
```

* 2.创建ElasticSearch索引,设置mapping

```    
    @Autowired
    private ElasticSearchProvider searchProvider;
    
    public void createIndex(){
        searchProvider.createIndex("orderindex",OrderVo.class);
    }
    
  ```

* 3.原生api操作

```
   @Autowired
    private ElasticSearchProvider searchProvider;
    //spring-data-elasticsearch 关于ElasticsearchRestTemplate操作类库
    searchProvider.getElasticsearchTemplate();
    //spring-data-elasticsearch 关于RestHighLevelClient操作类库
    searchProvider.getElasticsearchClient();

```