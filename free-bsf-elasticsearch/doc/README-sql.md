# ElasticSearch sql支持

### 介绍
ElasticSearch sql 支持需要安装es sql插件,组件内部可以使用druid或者rest模式进行操作。
es sql 有官方版本和一些开源版本,目前开源版本貌似已经暂停更新。
详细参考资料:https://github.com/NLPchina/elasticsearch-sql
### 使用说明
* es集群管理界面: http://{es服务器地址}:9100/
* sql插件地址: http://{es服务器地址}:9200/_nlpcn/sql
* sql插件可视化地址: http://{es服务器地址}:8080/

### 快速上手
* es 集群管理 快速界面调试支持,方便运维/开发人员调试,快速验证。
![Image text](es-server可视化.png)

* es sql 快速界面调试支持,方便开发人员调试,快速验证。
类似mysql的客户端管理界面。
![Image text](es-sql-可视化.png)


