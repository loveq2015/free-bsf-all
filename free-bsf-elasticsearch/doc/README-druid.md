# ElasticSearch druid模式集成

### 介绍
ElasticSearch采用druid的连接池方式进行操作。因mybatis plus需要采用druid新版本（时间类型支持）,故对es-sql进行改造,以支持jdk8时间类型。
[es-sql对druid v1.2.5+的改造支持](https://gitee.com/chejiangyi/elasticsearch-sql-for-druid/)

### 配置说明
连接ElasticSearch配置
注意: 因为druid版本问题,druid模式暂时不能使用(除非采用以上改造过的elasticsearch-sql-for-druid包),优先采用rest模式。
```
### 配置ES服务地址多个逗号隔开（使用原生的方式连接ES,也支持druid模式）
bsf.elasticsearch.server = 127.0.0.1
###  ES批提交最小数量, 默认5
bsf.elasticsearch.bulkSize = 5
```

### 代码示例
[es-sql语法参考,调试可视化参考](README-sql.md)
[索引相关操作,请看原生操作](README-common.md)
* 增删改查-使用ElasticSearchSqlProvider（查询推荐使用该组件-支持原生SQL语法）
```    
    @Autowired
    private ElasticSearchDruidSqlProvider searchProvider;
    
    /*
     * 批量保存数据
     */
    public void insert() {
        List<OrderVo> orderList = new ArrayList<OrderVo>();
        
        OrderVo order = new OrderVo();
        order.setOrderId(1000001);
        order.setOrderNo("ON1000001");
        order.setProductName("苹果水果新鲜当季整箱陕西红富士应季");
        order.setCreateBy("刘某某");
        order.setAmount(new BigDecimal(1024.00));
        order.setOrderPostion(new GeoPoint(30.0f, 20.0f));
        order.setOrderDesc("哈哈我也不知道要写什么苹果,反正先来个50斤吧");
        orderList.add(order);
        
        order = new OrderVo();
        order.setOrderId(1000002);
        order.setOrderNo("ON1000002");
        order.setProductName("四川丑橘新鲜水果当季整箱10斤大果丑八怪橘子丑桔丑柑耙耙柑包邮");
        order.setCreateBy("车某某");
        order.setAmount(new BigDecimal(888.88));
        order.setOrderPostion(new GeoPoint(70.0f, 60.0f));
        order.setOrderDesc("要新鲜的甜的丑橘，不要发错了！！！！");
        orderList.add(order);
        
        searchProvider.insertData("orderindex", "ordertype", orderList);
    }
    
   /**
     * 模糊搜索商品名称匹配苹果的数据,返回的数据按相关性得分从高到低排序并且取前3条数据
     * 更多的sql语法请参考官方示例 https://github.com/NLPchina/elasticsearch-sql
     * Elasticsearch-sql官方提供了一个用于测试sql的网页客户端,非常方便测试sql的写法
     */
    public List<OrderVo> search() {
        List<OrderVo> orderList = provider.searchBySql("select * from orderindex where productName = matchQuery('苹果') ORDER BY _score DESC LIMIT 3", OrderVo.class);
        return orderList;
    }
    
   /**
     * 普通查询写法 
     */
    public List<OrderVo> selectByIndex() {
        List<OrderVo> orderList = provider.searchBySql("select * from orderindex where createBy = '车某某' and amount > 500 LIMIT 3", OrderVo.class);
        return orderList;
    }
    
   /**
     *  查询type写法
     *  SELECT * FROM indexName
     */
    public List<OrderVo> selectByType() {
        List<OrderVo> orderList = provider.searchBySql("select * from orderindex where createBy = '刘某某' and amount > 500 LIMIT 3", OrderVo.class);
        return orderList;
    }
    
    /**
     * 删除写法
     */
    public void delete() {
        provider.deleteBySql("delete from orderindex where orderId = 1000001");
    }
  ```

### 性能压测报告
暂无