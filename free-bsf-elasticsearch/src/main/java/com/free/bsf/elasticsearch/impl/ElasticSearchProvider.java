package com.free.bsf.elasticsearch.impl;

import com.free.bsf.elasticsearch.ElasticSearchProperties;
import com.free.bsf.elasticsearch.base.ElasticSearchException;
import lombok.Getter;
import lombok.val;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;

import java.util.*;


public class ElasticSearchProvider implements AutoCloseable {
    @Getter
    protected ElasticsearchRestTemplate elasticsearchTemplate;
    @Getter
    protected RestHighLevelClient elasticsearchClient;

    public ElasticSearchProvider(RestHighLevelClient elasticsearchClient){
        this.elasticsearchClient=elasticsearchClient;
        this.elasticsearchTemplate=new ElasticsearchRestTemplate(elasticsearchClient);
    }

    public <T> void createIndex(String index,Class<T> tClass) {
        ElasticSearchMonitor.hook().run("createIndex", () -> {
            try {
                val indexCoordinate = this.elasticsearchTemplate.indexOps(IndexCoordinates.of(index));
                if(!indexExist(index)){
                    indexCoordinate.create();
                }
                val document = indexCoordinate.createMapping(tClass);
                val created = indexCoordinate.putMapping(document);
                if(!created){
                    throw new ElasticSearchException("索引创建失败"+index);
                }
            }catch (Exception e){
                throw new ElasticSearchException(e);
            }
        });
    }

    /**
     * 功能描述：删除索引
     *
     * @param index 索引名
     */
    public void deleteIndex(String index) {
        ElasticSearchMonitor.hook().run("deleteIndex", () -> {
            if (indexExist(index)) {
                try {
                    val op = this.elasticsearchTemplate.indexOps(IndexCoordinates.of(index)).delete();
                    if(!op)
                        throw new ElasticSearchException("failed to delete index.");
                }catch (Exception e){
                    throw new ElasticSearchException(e);
                }
            } else {
                throw new ElasticSearchException("index name not exists");
            }
        });
    }

    /**
     * 功能描述：验证索引是否存在
     *
     * @param index 索引名
     */
    public boolean indexExist(String index) {
        return ElasticSearchMonitor.hook().run("indexExist", () -> {
            try {
                boolean flag = elasticsearchTemplate.indexOps(IndexCoordinates.of(index)).exists();
                return flag;
            }catch (Exception e){
                throw new ElasticSearchException(e);
            }
        });
    }

    public <T> void insertData(String index, Collection<T> coll) {
        ElasticSearchMonitor.hook().run("insertData", () -> {
            try {
                if (coll.size() < ElasticSearchProperties.getBulkSize()) {
                    for(val item :coll){
                        this.elasticsearchTemplate.save(item,IndexCoordinates.of(index));
                    }
                } else {
                    this.elasticsearchTemplate.save(coll,IndexCoordinates.of(index));
                }
            }catch (Exception e){
                throw new ElasticSearchException(e);
            }
        });
    }

    @Override
    public void close(){

    }
}
