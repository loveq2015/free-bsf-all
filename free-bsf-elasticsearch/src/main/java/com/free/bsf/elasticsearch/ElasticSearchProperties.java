package com.free.bsf.elasticsearch;

import com.free.bsf.core.util.PropertyUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: chejiangyi
 * @version: 2019-08-12 12:17
 **/
public class ElasticSearchProperties {
    // es服务地址,多个用逗号分隔 默认端口9200
    public static String getServer(){
        return PropertyUtils.getPropertyCache(BsfElasticSearchServer,"");
    }

    public static Integer getRestPort(){
        return PropertyUtils.getPropertyCache("bsf.elasticsearch.restPort",9200);
    }

    public static Integer getTcpPort(){
        return PropertyUtils.getPropertyCache("bsf.elasticsearch.tcpPort",9300);
    }

    public static Integer getBulkSize(){
        return PropertyUtils.getPropertyCache(BsfElasticSearchBulkSize,5);
    }

    public static String Prefix="bsf.elasticsearch.";
    public static String Project="ElasticSearch";

    public final static String BsfElasticSearchAutoConfigurationImportFilterEnabled="bsf.elasticsearch.autoConfigurationImportFilter.enabled";
    public final static String BsfElasticSearchEnabled="bsf.elasticsearch.enabled";
    public final static String BsfElasticSearchServer="bsf.elasticsearch.server";

    public final static String BsfElasticSearchTcpPort="bsf.elasticsearch.tcpPort";
    public final static String BsfElasticSearchBulkSize="bsf.elasticsearch.bulkSize";
    public final static String BsfElasticSearchPrintSql="bsf.elasticsearch.printSql";
    public final static String SpringApplicationName="spring.application.name";
    public final static String ManagementHealthElasticSearchEnabled = "management.health.elasticsearch.enabled";
}
