package com.free.bsf.core.base;

import lombok.Getter;

public class BsfApplicationStatus {
    public static StatusEnum Current = StatusEnum.NONE;

    public enum StatusEnum{
        NONE,
        STARTED,
        STARTING,
        RUNNING,
        FAILED,
        STOPPING,
        STOPPED,
    }

    public interface StatusListener{
         void onApplicationEvent(StatusEnum event);
    }
}
