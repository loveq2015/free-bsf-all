package com.free.bsf.core.util;

import lombok.val;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * @author: chejiangyi
 * @version: 2019-07-24 11:00
 **/
public class StringUtils {
    public static String nullToEmpty(Object str) {
        return str != null ? str.toString() : "";
    }
    public static boolean isEmpty(String str){
        if(str ==null || str.isEmpty())
        {return true;}
        return false;
    }

    /**
     * 部分字符串获取
     */
    public static String subString2(String str, int maxlen)
    {
        if (org.springframework.util.StringUtils.isEmpty(str))
        { return str;}
        if (str.length() <= maxlen)
        {    return str;}
        return str.substring(0,maxlen);
    }

    /**
     * 部分字符串获取 超出部分末尾...
     */
    public static String subString3(String str, int maxlen)
    {
        if (org.springframework.util.StringUtils.isEmpty(str))
        { return str;}
        if (str.length() <= maxlen)
        {    return str;}
        return str.substring(0,maxlen)+"...";
    }

    public static String trimLeft(String str,char trim){
       return org.springframework.util.StringUtils.trimLeadingCharacter(str,trim);
    }
    public static String trimRight(String str,char trim){
        return org.springframework.util.StringUtils.trimTrailingCharacter(str,trim);
    }
    public static String trim(String str,char trim){
        return trimRight(trimLeft(str,trim),trim);
    }

    public static String[] spilt(String str,String spiltChar){
        if(isEmpty(str))
            return new String[]{};
        return str.split(spiltChar);
    }

    /**
    * ,分割,*做模糊匹配的 条件匹配算法
     * */
    public static boolean hitCondition(String condition,String data){
        if(StringUtils.isEmpty(condition)||data == null)
            return false;

        String[] skipUrls = condition.split(",");
        String trimChar="*";
        for(val skip : skipUrls) {
            //匹配所有
            if("*".equals(skip)){
                return true;
            }
            String trimUrl=StringUtils.trim(skip,'*');
            if(!skip.startsWith(trimChar)&&!skip.endsWith(trimChar)){
                if(data.equals(trimUrl))
                    return true;
            }
            else if(skip.startsWith(trimChar)&&skip.endsWith(trimChar)){
                if(data.contains(trimUrl))
                    return true;
            }else if(skip.startsWith(trimChar)){
                if(data.endsWith(trimUrl))
                    return true;
            }else if(skip.endsWith(trimChar)){
                if(data.startsWith(trimUrl))
                    return true;
            }
        }
        return false;
    }

    public static byte[] toArrays(InputStream input) throws IOException {
        try(ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int n = 0;
            while (-1 != (n = input.read(buffer))) {
                output.write(buffer, 0, n);
            }
            output.flush();
            return output.toByteArray();
        }
    }

    public static byte[] toArrays(String str){
        return str.getBytes(StandardCharsets.UTF_8);
    }

    public static String toString(byte[] bytes){
        return new String(bytes,StandardCharsets.UTF_8);
    }
}
