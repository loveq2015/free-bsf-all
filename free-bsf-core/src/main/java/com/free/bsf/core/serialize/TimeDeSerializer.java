package com.free.bsf.core.serialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.free.bsf.core.util.DateUtils;
import lombok.val;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Classname DateDeSerializer
 * @Description TODO
 * @Date 2021/4/16 18:30
 * @Created by chejiangyi
 */
public class TimeDeSerializer {
    protected static class BaseDeserializer<T> extends JsonDeserializer<T>{
        protected String format;
        public BaseDeserializer(String format){
            this.format = format;
        }
        @Override
        public T deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            return null;
        }
    }
    public static class DateDeserializer extends BaseDeserializer<Date> {
        public DateDeserializer(String format){
            super(format);
        }
        @Override
        public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            String source = p.getText().trim();
            if ("".equals(source)) {
                return null;
            }
            val date = DateUtils.strToDate(source,format);
            if(date ==null)
            {
                throw new IllegalArgumentException("Invalid Date value '" + source + "'");
            }
            return date;
        }

    }
    public static class LocalDateDeserializer extends BaseDeserializer<LocalDate> {
        public LocalDateDeserializer(String format){
            super(format);
        }
        @Override
        public LocalDate deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            String source = p.getText().trim();
            if ("".equals(source)) {
                return null;
            }
            val date = DateUtils.strToDate(source,format);
            if(date ==null)
            {
                throw new IllegalArgumentException("Invalid LocalDate value '" + source + "'");
            }
            return DateUtils.date2LocalDate(date);
        }

    }
    public static class LocalDateTimeDeserializer extends BaseDeserializer<LocalDateTime> {
        public LocalDateTimeDeserializer(String format){
            super(format);
        }
        @Override
        public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            String source = p.getText().trim();
            if ("".equals(source)) {
                return null;
            }
            val date = DateUtils.strToDate(source,format);
            if(date ==null)
            {
                throw new IllegalArgumentException("Invalid LocalDateTime value '" + source + "'");
            }
            return DateUtils.date2LocalDateTime(date);
        }

    }
}
