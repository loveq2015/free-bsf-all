package com.free.bsf.core.base;

import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.ReflectionUtils;
import lombok.val;

/**
 * @author: chejiangyi
 * 默认环境枚举:包含dev,fat,uat,pro环境
 * @version: 2019-05-27 13:38
 **/
public enum  BsfEnvironmentEnum {
    APOLLO_DEV(Environment.dev,ReflectionUtils.tryGetStaticFieldValue("com.lmc.bsf.core.base.BsfBaseConfig","apollo_dev",""),"bsf_apollo_url"),
    APOLLO_Test(Environment.test,ReflectionUtils.tryGetStaticFieldValue("com.lmc.bsf.core.base.BsfBaseConfig","apollo_test",""),"bsf_apollo_url"),
    APOLLO_PRE(Environment.pre,ReflectionUtils.tryGetStaticFieldValue("com.lmc.bsf.core.base.BsfBaseConfig","apollo_pre",""),"bsf_apollo_url"),
    APOLLO_PRO(Environment.pro,ReflectionUtils.tryGetStaticFieldValue("com.lmc.bsf.core.base.BsfBaseConfig","apollo_pro",""),"bsf_apollo_url");

    private Environment env;
    private String url;
    private String serverkey;

    BsfEnvironmentEnum(Environment env,String url,String serverkey)
    {
        this.env = env;
        this.url = url;
        this.serverkey = serverkey;
    }

    public Environment getEnv() {
        return env;
    }

    public String getUrl() {
        return url;
    }

    public String getServerkey() {
        return serverkey;
    }

    public static BsfEnvironmentEnum get(String serverKey,BsfEnvironmentEnum defaultValue){
        for(BsfEnvironmentEnum e:BsfEnvironmentEnum.values()){
            if(e.getServerkey().equalsIgnoreCase(serverKey)&& e.getEnv().toString().equalsIgnoreCase(PropertyUtils.getPropertyCache(CoreProperties.BsfEnv,""))){
                return e;
            }
        }
        return defaultValue;
    }
}
