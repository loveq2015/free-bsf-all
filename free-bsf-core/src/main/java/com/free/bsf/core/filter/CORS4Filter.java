package com.free.bsf.core.filter;

import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class CORS4Filter implements Filter {
    public CORS4Filter() {
    }
    private final String AccessControlRequestMethod="Access-Control-Request-Method";
    private final String AccessControlAllowMethods="Access-Control-Allow-Methods";
    private final String AccessControlRequestHeaders="Access-Control-Request-Headers";
    private final String AccessControlAllowHeaders="Access-Control-Allow-Headers";
    private final String AccessControlAllowOrigin="Access-Control-Allow-Origin";
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse res = (HttpServletResponse)response;
        HttpServletRequest req = (HttpServletRequest)request;
        if (!res.containsHeader("Access-Control-Allow-Headers")) {
            res.addHeader("Access-Control-Allow-Credentials", "true");
            if(req.getHeader(AccessControlRequestMethod)!=null){
                res.addHeader(AccessControlAllowMethods, req.getHeader(AccessControlRequestMethod));
            }else {
                res.addHeader(AccessControlAllowMethods, "*");
            }
            if(req.getHeader(AccessControlRequestHeaders)!=null) {
                res.addHeader(AccessControlAllowHeaders, req.getHeader(AccessControlRequestHeaders));
            }else{
                res.addHeader(AccessControlAllowHeaders, "*");
            }
            if (req.getHeader("Origin")!=null) {
                res.addHeader(AccessControlAllowOrigin, req.getHeader("Origin"));
            } else {
                res.addHeader(AccessControlAllowOrigin, "*");
            }
            if ("OPTIONS".equals(req.getMethod().toUpperCase())) {
                res.setStatus(HttpStatus.NO_CONTENT.value());
            } else {
                chain.doFilter(request, response);
            }
        }
    }

    public void destroy() {
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }
}