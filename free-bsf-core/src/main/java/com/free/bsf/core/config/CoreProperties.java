package com.free.bsf.core.config;

import com.free.bsf.core.base.Environment;

import com.free.bsf.core.util.PropertyUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: chejiangyi
 * @version: 2019-08-12 11:27
 **/
public class CoreProperties {
    /**
     * 枚举:dev,test,pre,pro
     */
    public static Environment getEnv(){
        return Environment.valueOf(PropertyUtils.getPropertyCache("bsf.env","dev"));
    }
    public static String getApplicationName(){
        return PropertyUtils.getPropertyCache(SpringApplicationName,"");
    }

    public static String Project="Core";
    public static String SpringApplicationName= "spring.application.name";
    public static String BsfEnv="bsf.env";
    public static String SpringJacksonDateFormat="spring.jackson.date-format";
    public static String SpringJacksonTimeZone="spring.jackson.time-zone";
    public static String ServerTomcatMaxThreads="server.tomcat.max-threads";
    public static String ServerTomcatMaxConnections="server.tomcat.max-connections";
    public static String ServerTomcatMinSpaceThreads="server.tomcat.min-spare-threads";
    public static String ServeCompressionEnabled="server.compression.enabled";
    public static String ServeCompressionMimeTypes="server.compression.mime-types";
    public static String LoggingFileName="logging.file.name";
    public static String LoggingFileMaxHistory="logging.file.max-history";
    public static String LoggingFileMaxSize="logging.file.max-size";
    public static String BsfLoggingFileTotalSize="bsf.logging.file.total-size";
    public static String BsfContextRestartText="bsf.context.restart.text";
    public static String BsfContextRestartEnabled="bsf.context.restart.enabled";
    public static String BsfContextRestartTimeSpan="bsf.context.restart.timespan";
    public static String BsfEnabled = "bsf.enabled";
    public static String BsfCollectHookEnabled="bsf.collect.hook.enabled";
    public static String BsfIsPrintSqlTimeWatch="bsf.db.printSqlTimeWatch.enabled";
    public static String BsfIsPrintSqlError="bsf.db.printSqlError.enabled";
    public static String BsfJsonIgnoreCaseEnabled="bsf.json.ignoreCase.enabled";
    public static String BsfJsonDateTime="bsf.json.dateTime.enabled";
    public static String BsfJsonIncludeNotNull="bsf.json.includeNotNull.enabled";
    public static String BsfJsonJDK8="bsf.json.jdk8.enabled";
    public static String BsfJsonDateFormat="bsf.json.date-format";
    public static String BsfConvertDateFormat="bsf.convert.date-format";
    public static String BsfNetworkIP= "bsf.network.ip";

    public static Integer BsfProtostuffSchemaMaxCacheSize(){
        return PropertyUtils.getPropertyCache("bsf.protostuff.schema.maxCacheSize",2000);
    }
    public static final String BsfWebSerializeProtostuffEnabled="bsf.web.serialize.protostuff.enabled";
}
