# core 进阶篇
#### 依赖引用
bsf-core 是bsf最核心的框架,所有bsf组件都必须引用这个组件。
默认项目脚手架,若引用bsf-starter,则默认已引用此依赖,无需额外引用。
``` 
<dependency>
	<artifactId>free-bsf-core</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7.1-SNAPSHOT</version>
</dependency>
```
### 环境变量标准
标准环境定义为dev(开发),fat(测试),uat(预发),pro(生产)环境
``` 
#spring profile 环境变量配置,默认dev启动
spring.profiles.active=${env:dev}

#bsf 环境变量配置【必须】
bsf.env=${spring.profiles.active}
``` 

### json标准
默认使用jackson为业务及框架序列化标准。默认请使用JsonUtils工具类,支持JDK8 LocalDateTime等jdk8类型,支持多种时间格式反序列化兼容,开发人员直接使用。
``` 
#json 默认时间格式规范
spring.jackson.date-format = yyyy-MM-dd HH:mm:ss

#json 默认时区规范
spring.jackson.time-zone = GMT+8

#json 默认时间序列化支持
bsf.json.dateTime.enabled=true

#json 默认jk8支持
bsf.json.jdk8.enabled=true

#json 支持序列化时去掉null,json会更小
bsf.json.includeNotNull.enabled=true

#json 默认时间反序列化格式支持(兼容多时间格式)
bsf.convert.dateformat=yyyy-MM-dd HH:mm:ss,yyyy-MM-dd'T'HH:mm:ss,yyyy-MM-dd,yyyyMMddHHmmss

#支持http(spring)序列化采用默认的序列化,注意:需要手工开启
bsf.web.serialize3.enabled=true
``` 

### http json等输出标准
```
#采用bsf JsonUtils为标准序列化,支持json协议
bsf.web.serialize3.enabled=false
#采用bsf Protostuff为标准的序列化支持,支持protobuf协议,根据header中Content-Type:application/x-protobuf触发
bsf.web.serialize.protostuff.enabled=false
```

### tomcat web 容器优化标准
默认使用bsf框架,自动进行优化,如非特殊场景需求,开发人员无需关心。
```
#tomcat 默认最大连接池大小
server.tomcat.max-threads=200*核心数

#tomcat 默认最大连接数大小(连接队列最大数)
server.tomcat.max-connections=10000*核心数

#tomcat 默认最小空闲线程池大小
server.tomcat.min-spare-threads=20*核心数/2

#web容器默认http开启压缩支持
server.compression.enabled = true

#web容器默认http压缩支持的类型
server.compression.mime-types = application/json,application/xml,text/html,text/xml,text/plain
``` 

### 日志标准
默认使用logback,日志会自动清理,自动同步到elk,开发人员无需关心。
``` 
#默认日志的保存目录
logging.file=log/app.log

#默认日志保留n天
logging.file.max-history=3

#默认单个文件最大大小
logging.file.max-size=10M

#默认日志文件总大小(超过自动删除)
bsf.logging.file.total-size=1G
``` 

### 跨域标准
```
#跨域支持（旧版本,不建议使用）
bsf.web.cors3.enabled=false
#支持cookie的跨域使用(旧版本,不建议使用)
bsf.web.cors3.cookie.enabled=false
```
```
#新版本跨域支持,也同时支持cookie(对于严格的跨域浏览器客户端,不再支持*模式匹配,对此进行优化)
bsf.web.cors4.enabled=true
```

### bsf层面ip限制
```
#剔除部分ip筛选,格式:10.42,172
bsf.util.ip.exclude.regex=
bsf.util.ip.include.regex=
```

### 常用通用工具类
* HttpClientUtils: http client类库  
有内部封装http连接池,性能好  
[HttpClientUtils](README-HttpClientUtils.md)
* WebUtils: 获取web上下文 
可以业务任意地方获取上下文
```
    //获取web上下文
     WebUtils.getContext();
    //获取web请求
     WebUtils.getRequest();
    //获取web请求返回
     WebUtils.getResponse();
```
* BeanCopyUtil: 类属性拷贝类 
支持属性拷贝,未来增加缓存,未来持续优化性能
* ContextUtils: 应用容器上下文类,获取bean等
* ConvertUtils: 类型转换类
可扩展不同类型的转化
* JsonUtils: 标准json序列化封装,简单易用,封装所有能想到的场景及日期兼容反序列化。
```
//序列化
JsonUtils.serialize(object);
//反序列化
JsonUtils.deserialize(type);
//复杂类型反序列化,如泛型
val type = new TypeReference<Object>() {
    @Override
    public Type getType() {
        return method.getReturnType();
    }
}
JsonUtils.deserialize(type);
```
* PropertyUtils: 配置读取及缓存,性能好
* ThreadUtils: 内部线程池,自动弹性伸缩,封装并行操作,性能好
```
//bsf 底层全局可伸缩的连接池,性能好
ThreadUtils.system()
//高性能并行循环,阻塞等所有任务结束后继续执行。
ThreadUtils.parallelFor("并行测试", 10,taskList, (c)->{???})
//进程关闭或杀死时执行
ThreadUtils.shutdown(()->{???},1,false);
``` 
* DbHelper: 数据库直连操作库,可以使用sql直接操作数据库，简单方便;支持事务！
``` 
//操作使用数据库操作,不支持事务传递,无返回值
DbHelper.call(DataSource dataSource,Callable.Action1<DbConn> action0)
//操作使用数据库操作,不支持事务传递,有返回值
DbHelper.get(DataSource dataSource,Callable.Func1<T,DbConn> action0)
//开启事务,定义事务隔离级别
DbHelper.transaction(DataSource dataSource, int level, Callable.Action0 action0)
//操作使用数据库操作,支持事务传递,无返回值
DbHelper.transactionCall(DataSource dataSource,Callable.Action1<DbConn> action0)
//操作使用数据库操作,支持事务传递,有返回值
DbHelper.transactionGet(DataSource dataSource,Callable.Func1<T,DbConn> action0)
``` 

### bsf生命周期拦截
通过生命周期拦截,业务可以做一些启动初始化和关闭前释放资源的事情。
```
@Bean
public BsfApplicationStatus.StatusListener applicationStatusListener(){
    return new BsfApplicationStatus.StatusListener() {
        @Override
        public void onApplicationEvent(BsfApplicationStatus.StatusEnum statusEnum) {
            //可以拦截started,running(程序已启动运行中),stopping(程序停止前),stopped这几种状态!
            System.out.println(statusEnum.name());
        }
    };
}
```