package com.free.bsf.apiregistry.loadbalance;

import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.base.ApiClientInfo;
import com.free.bsf.apiregistry.base.ApiUtils;
import com.free.bsf.apiregistry.registry.BaseRegistry;
import com.free.bsf.core.base.Ref;
import com.free.bsf.core.util.*;
import lombok.val;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 负载均衡器定义
 */
public class BaseLoadBalance {

    public BaseLoadBalance(){
        ThreadUtils.system().submit("apiRegistry 故障恢复定时任务",()->{
            while (!ThreadUtils.system().isShutdown()) {
                if(failsList.size()>0) {
                    synchronized (failsListLock) {
                        failsList.clear();
                    }
                }
                ThreadUtils.sleep(ApiRegistryProperties.getRegistryFailRetryTimeSpan());
            }
        });
    }
    /**可用负载均衡节点*/
    public String getAvailableHostPort(String appName){
        return null;
    }
    /**添加失败节点*/
    public void addFail(String appName,String hostPort){
        boolean isConnect=isConnectHostPort(hostPort);
        if(!isConnect) {
            synchronized (failsListLock) {
                if (!failsList.containsKey(appName)) {
                    failsList.put(appName, new ArrayList<>());
                }
                val list = failsList.get(appName);
                if (!list.contains(hostPort))
                    list.add(hostPort);
            }
        }
    }

    public static boolean isConnectHostPort(String hostPort){
        val url = UriComponentsBuilder.fromUriString(ApiUtils.getUrl(hostPort,"")).build();
        return NetworkUtils.isHostConnectable(url.getHost(),url.getPort()<=0?80:url.getPort(),5000);
    }

    protected ConcurrentHashMap<String,List<String>> failsList =new ConcurrentHashMap<>();
    protected Object failsListLock = new Object();

    protected List<String> getAvailableHostPortList(String appName){
        List rs = new ArrayList<String>();
        //配置优先
        for(val s : ApiRegistryProperties.getAppNameHosts(appName)){
            if(!StringUtils.isEmpty(s))
            {rs.add(s);}
        }
        if(rs.size()>0){
            return rs;
        }
        //注册中心
        val registry = ContextUtils.getBean(BaseRegistry.class,false);
        if(registry!=null) {
            val list = registry.getServerList().get(appName);
            if(list!=null) {
                for (val s : list) {
                    if (!StringUtils.isEmpty(s)) {
                        rs.add(s);
                    }
                }
            }
        }
        //移除错误
        val fails = failsList.get(appName);
        if(fails!=null){
            rs.removeAll(fails);
        }
        return rs;
    }

    public static void main(String[] args){
        String[] ports = new String[]{"10.70.0.91:8080","http://10.70.0.91:8080/aaa"
                ,"http://www.baidu.com/aa","http://inner-114.linkmore.com/"
                ,"inner-114.linkmore.com"};
        for(int i=0;i<1000;i++) {
            for (val p : ports) {
                Ref<Boolean> r = new Ref<>(false);
                long time = TimeWatchUtils.time(()->{
                     r.setData(BaseLoadBalance.isConnectHostPort(p));
                });

                System.out.println(p + " -> " + r +" :time "+time);
            }
        }
    }
}
