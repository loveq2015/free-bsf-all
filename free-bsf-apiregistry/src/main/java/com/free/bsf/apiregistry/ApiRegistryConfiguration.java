package com.free.bsf.apiregistry;

import com.free.bsf.apiregistry.apiclient.Aspects;
import com.free.bsf.apiregistry.base.ApiRegistryApplicationRunner;
import com.free.bsf.apiregistry.base.ApiRegistryHealthFilter;
import com.free.bsf.apiregistry.loadbalance.BaseLoadBalance;
import com.free.bsf.apiregistry.loadbalance.LoadBalanceFactory;
import com.free.bsf.apiregistry.registry.BaseRegistry;
import com.free.bsf.apiregistry.registry.RegistryFactory;
import com.free.bsf.apiregistry.scan.ApiClientPackageScan;
import com.free.bsf.core.config.BsfConfiguration;
import com.free.bsf.core.util.LogUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;

/**
 * @version : 2022-12-7 14:30
 * @author: chejiangyi * 
 * @date 2022-12-7
 **/
@Configuration
@ConditionalOnProperty(name = ApiRegistryProperties.Enabled, havingValue = "true")
@Import(value = { BsfConfiguration.class,ApiClientPackageScan.ApiClientAnnotationPackageScan.class })
public class ApiRegistryConfiguration {


	@ConditionalOnProperty(name = ApiRegistryProperties.ApiClientAspectEnabled, havingValue = "true")
	@Bean
	@ConditionalOnClass(name = {"org.aspectj.lang.annotation.Aspect",ApiRegistryProperties.FeignClientClassPath})
	public Aspects.AllClientAspect allClientAspect(){
		return new Aspects.AllClientAspect();
	}
	@ConditionalOnProperty(name = ApiRegistryProperties.ApiClientAspectEnabled, havingValue = "true")
	@Bean
	@ConditionalOnClass(name = {"org.aspectj.lang.annotation.Aspect"})
	@ConditionalOnMissingClass(value=ApiRegistryProperties.FeignClientClassPath)
	public Aspects.ApiClientAspect apiClientAspect(){
		return new Aspects.ApiClientAspect();
	}

	@Bean
	public BaseLoadBalance getBaseLoadBalance() {
		return LoadBalanceFactory.create();
	}

	@Bean
	public BaseRegistry getBaseRegistry() {
		return RegistryFactory.create();
	}

	@Bean
	public ApiRegistryApplicationRunner getApiRegistryApplicationRunner() {
		return new ApiRegistryApplicationRunner();
	}

	@ConditionalOnProperty(name = ApiRegistryProperties.HealthEnabled, havingValue = "true",matchIfMissing = true)
	@Bean
	@ConditionalOnWebApplication
	public FilterRegistrationBean apiRegistryStatusFilter() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		// 优先注入
		filterRegistrationBean.setOrder(Ordered.LOWEST_PRECEDENCE-1);
		filterRegistrationBean.setFilter(new ApiRegistryHealthFilter());
		filterRegistrationBean.setName("bsfApiRegistryStatusFilter");
		filterRegistrationBean.addUrlPatterns("/bsf/*");
		LogUtils.info(ApiRegistryConfiguration.class, ApiRegistryProperties.Project,
				"apiRegistry status filter注册成功!");
		return filterRegistrationBean;
	}
}
