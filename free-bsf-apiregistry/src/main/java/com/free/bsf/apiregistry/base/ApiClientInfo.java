package com.free.bsf.apiregistry.base;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiClientInfo {
    String name;
    String path;
}
