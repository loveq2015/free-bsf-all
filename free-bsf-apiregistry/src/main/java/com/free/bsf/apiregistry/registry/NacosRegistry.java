package com.free.bsf.apiregistry.registry;

import com.free.bsf.api.apiregistry.INacosRegistry;
import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.base.ApiRegistryException;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.*;
import lombok.val;
import redis.clients.jedis.params.SetParams;

import java.util.*;

/**
 * Redis注册中心实现
 */
public class NacosRegistry extends BaseRegistry {

    private Map<String, List<String>> cacheServerList = new HashMap<>();

    public NacosRegistry() {
        super();
    }

    private INacosRegistry getNacosRegistryImpl(){
        val o = ContextUtils.tryGetBean(INacosRegistry.class);
        if(o==null){
            throw new ApiRegistryException("nacos未开启或未加载实现包");
        }
        return o;
    }

    @Override
    public void register() {
        super.register();
        getNacosRegistryImpl().register();
        ThreadUtils.system().submit("apiRegistry nacos心跳获取服务列表", () -> {
            while (!ThreadUtils.system().isShutdown()) {
                try {
                    heartBeat();
                } catch (Exception e) {
                    LogUtils.error(RedisRegistry.class, ApiRegistryProperties.Project, "nacos心跳获取服务列表出错");
                }
                ThreadUtils.sleep(ApiRegistryProperties.getRegistryNacosServerListCacheHeartBeatTime());
            }
        });
    }

    private void heartBeat() {
        val list =getNacosRegistryImpl().getServerList();
        if(list!=null){
            cacheServerList = list;
        }
    }

    @Override
    public Map<String, List<String>> getServerList() {
        return cacheServerList;
    }

    @Override
    public String getReport() {
        StringBuilder sb = new StringBuilder();
        for (val kv : cacheServerList.entrySet()) {
            sb.append("服务:" + kv.getKey() + "\r\n");
            for (val server : kv.getValue()) {
                sb.append("    " + server + "\r\n");
            }
        }
        return sb.toString();
    }

    @Override
    public void close() {
        super.close();
        val registry = ContextUtils.tryGetBean(INacosRegistry.class);
        if(registry!=null) {
            registry.close();
        }
    }
}
