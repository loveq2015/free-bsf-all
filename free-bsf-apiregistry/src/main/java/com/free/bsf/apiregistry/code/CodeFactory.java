package com.free.bsf.apiregistry.code;

import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.base.ApiRegistryException;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.val;

import java.util.HashMap;
import java.util.Map;

public class CodeFactory {

    public static ICode create(String contentType){
        contentType = StringUtils.nullToEmpty(contentType).toLowerCase();
        if(contentType.contains("json")){
            return new JsonCode();
        }else if(contentType.contains("protostuff")||contentType.contains("protobuf")){
            return new ProtostuffCode();
        }
        return create();
    }

    public static ICode create(){
        val codeType = ApiRegistryProperties.getRegistryCodeType();
        if("json".equalsIgnoreCase(codeType)){
            return new JsonCode();
        }else if("protostuff".equalsIgnoreCase(codeType)||"protobuf".equalsIgnoreCase(codeType)){
            return new ProtostuffCode();
        }
        throw new ApiRegistryException("请配置bsf.apiRegistry.code.type");
    }

    public static String getHeader(){
        val codeType = ApiRegistryProperties.getRegistryCodeType();
        val header = codeHeadersEnum.get(codeType);
        if(header==null){
            throw new ApiRegistryException("请配置bsf.apiRegistry.code.type");
        }
        return header;
    }

    private static Map<String,String> codeHeadersEnum = new HashMap<String,String>(){{
        put("json","application/json");
        put("protobuf","application/x-protobuf");
        put("protostuff","application/x-protobuf");
    }};
}
