package com.free.bsf.apiregistry.apiclient;

import com.free.bsf.apiregistry.base.ApiClientInfo;
import com.free.bsf.apiregistry.base.BaseApiClientParser;
import com.free.bsf.apiregistry.code.CodeFactory;
import com.free.bsf.core.apiregistry.RequestInfo;
import com.free.bsf.core.util.StringUtils;
import lombok.val;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @ApiClient注解解析器实现通用java springmvc http写法方式解析
 */
public class ApiClientParser extends BaseApiClientParser {
    public RequestInfo parse(ApiClientParserInfo info){
        RequestInfo requestInfo = new RequestInfo();
        requestInfo.setAppName(info.getAppName());
        buildHeader(requestInfo);
        buildMethod(requestInfo,info);
        buildBody(requestInfo,info);
        return requestInfo;
    }

    private void buildHeader(RequestInfo requestInfo){
        if(!requestInfo.getHeader().containsKey("Content-Type")) {
            requestInfo.getHeader().put("Content-Type", CodeFactory.getHeader());
        }
    }

    private void buildMethod(RequestInfo requestInfo,ApiClientParserInfo info){
        val method = info.getMethod();
        val postMapping = AnnotationUtils.getAnnotation(method, PostMapping.class);
        if(postMapping!=null){
            requestInfo.setMethod("POST");
            buildUrl(requestInfo,info,postMapping.value().length>0?postMapping.value()[0]:"");
            return;
        }
        val getMapping = AnnotationUtils.getAnnotation(method, GetMapping.class);
        if(getMapping!=null){
            requestInfo.setMethod("GET");
            buildUrl(requestInfo,info,getMapping.value().length>0?getMapping.value()[0]:"");
            return;
        }
        val reqMapping = AnnotationUtils.getAnnotation(method, RequestMapping.class);
        if(reqMapping!=null){
            requestInfo.setMethod(reqMapping.method().length>0?reqMapping.method()[0].name():"");
            buildUrl(requestInfo,info,reqMapping.value().length>0?reqMapping.value()[0]:"");
            return;
        }
    }

    private void buildUrl(RequestInfo requestInfo,ApiClientParserInfo info,String httpPath){
        var urlBuilder = UriComponentsBuilder.fromUriString(info.getUrl());
        val ps = info.getMethod().getParameters();
        for(Integer i=0;i<ps.length;i++){
            var p = ps[i];
            val param = AnnotationUtils.getAnnotation(p, RequestParam.class);
            if(param!=null){
                Object value = info.getJoinPoint().getArgs()[i];
                if(value == null && !ValueConstants.DEFAULT_NONE.equals(param.defaultValue())) {
                    value = param.defaultValue();
                }
                if(value!=null){
                    if(value instanceof Collection){
                        urlBuilder = urlBuilder.queryParam(param.value(),(Collection)value);
                    }else if(value.getClass().isArray()){
                        urlBuilder = urlBuilder.queryParam(param.value(), Arrays.asList(value));
                    }else{
                        urlBuilder = urlBuilder.queryParam(param.value(),value);
                    }
                }
            }

        }
        if(!StringUtils.isEmpty(httpPath))
        {urlBuilder.path(httpPath);}
        requestInfo.setUrl(urlBuilder.build().toUri().toString());
    }

    private void buildBody(RequestInfo requestInfo,ApiClientParserInfo info){
        val ps = info.getMethod().getParameters();
        for(Integer i=0;i<ps.length;i++){
            var p = ps[i];
            val requestBody = AnnotationUtils.getAnnotation(p, RequestBody.class);
            if(requestBody!=null){
                val value = info.getJoinPoint().getArgs()[i];
                val code = CodeFactory.create(requestInfo.getHeader().get("Content-Type"));
                requestInfo.setBody(code.encode(value));
                return;
            }
        }
    }
}
