package com.free.bsf.apiregistry.rpcclient;

import com.free.bsf.apiregistry.base.ApiRegistryException;
import com.free.bsf.apiregistry.base.ApiRegistryHttpStateException;
import com.free.bsf.apiregistry.code.CodeFactory;
import com.free.bsf.core.apiregistry.RequestInfo;
import com.free.bsf.core.util.HttpClientUtils;
import com.free.bsf.core.util.JsonUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.val;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.EntityUtils;

import java.lang.reflect.Type;

/**
 * httpClient实现
 */
public class HttpClientRpcClient implements IRpcClient {
    public  <T>T execute(RequestInfo requestInfo, Type cls){
        val r = httpClientRequest(requestInfo);
        try {
            try (val response = HttpClientUtils.system().getClient().execute(r)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new ApiRegistryHttpStateException(requestInfo.getAppName(),StringUtils.nullToEmpty(requestInfo.getUrl()),response.getStatusLine().getStatusCode());
                }
                val code = CodeFactory.create(JsonUtils.serialize(response.getHeaders("Content-Type")));
                return code.decode(EntityUtils.toByteArray(response.getEntity()),cls);
            }
        }catch (Exception e){
            throw new ApiRegistryException(e);
        }
    }

    protected HttpUriRequest httpClientRequest(RequestInfo requestInfo){
        RequestBuilder requestBuilder = RequestBuilder.create(requestInfo.getMethod());
        requestBuilder.setUri(requestInfo.getUrl());
        if(requestInfo.getBody()!=null) {
            requestBuilder.setEntity(new ByteArrayEntity(requestInfo.getBody()));
        }
        for(val h:requestInfo.getHeader().entrySet()){
            requestBuilder.addHeader(h.getKey(), h.getValue());
        }
        return requestBuilder.build();
    }

}
