package com.free.bsf.apiregistry.code;

import com.free.bsf.apiregistry.base.ApiRegistryException;
import com.free.bsf.core.util.ProtostuffUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ProtostuffCode implements ICode {
    public <T> byte[] encode(T data){
        return ProtostuffUtils.serialize(data);
    }
    public <T> T decode(byte[] data, Type type){
        if(type instanceof ParameterizedType){
            return ProtostuffUtils.deserialize(data,(Class<T>)((ParameterizedType)type).getRawType());
        }
        if(type instanceof Class<?>){
            return ProtostuffUtils.deserialize(data,(Class<T>)type);
        }
        throw new ApiRegistryException("Protostuff不支持该类型反序列化");
    }
}
