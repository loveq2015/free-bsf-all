package com.free.bsf.apiregistry.registry;

import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.base.ApiRegistryException;
import lombok.val;

public class RegistryFactory {
    public static BaseRegistry create(){
        val type = ApiRegistryProperties.getRegistryType();
        if(RedisRegistry.class.getSimpleName().equalsIgnoreCase(type)){
            return new RedisRegistry();
        }
        if(NacosRegistry.class.getSimpleName().equalsIgnoreCase(type)){
            return new NacosRegistry();
        }
        if(NoneRegistry.class.getSimpleName().equalsIgnoreCase(type)){
            return new NoneRegistry();
        }
        throw new ApiRegistryException("请配置bsf.apiRegistry.registry.type");
    }
}
