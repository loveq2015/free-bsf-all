package com.free.bsf.apiregistry.base;

import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.registry.BaseRegistry;
import com.free.bsf.core.base.BsfApplicationStatus;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.LogUtils;
import org.springframework.core.annotation.Order;

@Order(Integer.MAX_VALUE-1)
public class ApiRegistryApplicationRunner implements BsfApplicationStatus.StatusListener {
    @Override
    public void onApplicationEvent(BsfApplicationStatus.StatusEnum statusEnum) {
        if(statusEnum == BsfApplicationStatus.StatusEnum.RUNNING) {
            BaseRegistry registry = ContextUtils.getBean(BaseRegistry.class, false);
            if (registry != null) {
                LogUtils.info(ApiRegistryApplicationRunner.class, ApiRegistryProperties.Project, "ApiRegistry注册开启中.....");
                registry.register();
            }
        }
    }
}
