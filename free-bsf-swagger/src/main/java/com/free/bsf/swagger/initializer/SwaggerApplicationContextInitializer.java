package com.free.bsf.swagger.initializer;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy;
import ch.qos.logback.core.util.FileSize;
import com.free.bsf.core.base.BsfEnvironmentEnum;
import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.common.PropertyCache;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.*;
import com.free.bsf.swagger.config.SwaggerProperties;
import lombok.val;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.io.File;
import java.util.Random;
import java.util.UUID;

/**
 * @author: chejiangyi
 * @version: 2019-06-14 19:02
 **/
public class SwaggerApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext context) {
        ConfigurableEnvironment environment = context.getEnvironment();
        if("false".equalsIgnoreCase(environment.getProperty(CoreProperties.BsfEnabled))){
            return;
        }
        //环境变量初始化
        String propertyValue = environment.getProperty(CoreProperties.SpringApplicationName);
        if (!Strings.isEmpty(propertyValue)) {
            /**
             * 解决swagger版本在springboot 2.6+ 升级后的bug，未来可以取消
             */
            setDefaultProperty(SwaggerProperties.SpringMvcPathMatchMatchingStrategy, "ant-path-matcher", "[swagger版本兼容]");
        }
    }



    void setDefaultProperty(String key,String defaultPropertyValue,String message)
    {
        PropertyUtils.setDefaultInitProperty(SwaggerApplicationContextInitializer.class,SwaggerProperties.Project,key,defaultPropertyValue,message);
    }


}
