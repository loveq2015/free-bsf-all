package com.free.bsf.swagger.strategy;

import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.swagger.config.SwaggerProperties;
import lombok.val;

/**
 * @Classname SwaggerFactory
 * @Description Swagger 选择工厂
 * @Date 2021/4/1 19:10
 * @Created by chejiangyi
 */
public class SwaggerFactory {
    public static ISwagger chooseStrategy(){
        val swagger = ContextUtils.getBean(ISwagger.class,false);
        if(swagger!=null){
            return swagger;
        }
        else if("simple".equalsIgnoreCase(SwaggerProperties.getDefault().getStrategy())){
            return new SimpleSwagger();
        }
        return new SimpleSwagger();
    }
}
