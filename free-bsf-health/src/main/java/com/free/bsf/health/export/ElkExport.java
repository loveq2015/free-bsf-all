package com.free.bsf.health.export;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Context;
import com.free.bsf.core.base.BsfEnvironmentEnum;
import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.health.base.AbstractExport;
import com.free.bsf.health.base.Report;
import com.free.bsf.health.config.ExportProperties;
import com.free.bsf.health.config.HealthProperties;
import lombok.val;

import net.logstash.logback.appender.LogstashTcpSocketAppender;
import net.logstash.logback.encoder.LogstashEncoder;
import net.logstash.logback.marker.MapEntriesAppendingMarker;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author: chejiangyi
 * @version: 2019-08-13 19:48
 **/
public class ElkExport extends AbstractExport {

    private LogstashTcpSocketAppender appender;


    @Override
    public void start() {
        super.start();
        val log =LoggerFactory.getILoggerFactory();
        if(log!=null&&log instanceof Context) {
            appender = new LogstashTcpSocketAppender();
            appender.setContext((Context) log);
            var destinations = ExportProperties.getElkDestinations();
            if (destinations == null || destinations.length == 0) {
               throw new BsfException("bsf.elk.destinations 未配置");
            }
            for (String destination : destinations) {
                appender.addDestination(destination);
            }
            val encoder = new LogstashEncoder();
            val appname = "bsfReport-" + PropertyUtils.getPropertyCache(HealthProperties.SpringApplictionName, "");
            encoder.setCustomFields("{\"appname\":\"" + appname + "\",\"appindex\":\"bsfReport\"}");
            encoder.setEncoding("UTF-8");
            appender.setEncoder(encoder);
            appender.start();
        }
    }
    @Override
    public void run(Report report){
        if(appender == null||!ExportProperties.getElkEnabled())
        {
            return;
        }
        Map<String,Object> map= new LinkedHashMap();
        report.eachReport((String field, Report.ReportItem reportItem) -> {
            if(reportItem!=null && reportItem.getValue() instanceof Number) {
                map.put(field.replace(".", "_"), reportItem.getValue());
            }
            return reportItem;
        });
        val event = createLoggerEvent(map,"bsf Report:"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        appender.doAppend(event);
    }

    private LoggingEvent createLoggerEvent(Map<String, Object> values, String message){
        LoggingEvent loggingEvent = new LoggingEvent();
        loggingEvent.setTimeStamp(System.currentTimeMillis());
        loggingEvent.setLevel(Level.INFO);
        loggingEvent.setLoggerName("bsfReportLogger");
        loggingEvent.setMarker(new MapEntriesAppendingMarker(values));
        loggingEvent.setMessage(message);
        loggingEvent.setArgumentArray(new String[0]);
        loggingEvent.setThreadName(Thread.currentThread().getName());
        return loggingEvent;
    }

    @Override
    public void close() {
        super.close();
        if(appender!=null) {
            appender.stop();
        }
    }
}
