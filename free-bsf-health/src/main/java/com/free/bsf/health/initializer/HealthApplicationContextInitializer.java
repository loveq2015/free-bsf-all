package com.free.bsf.health.initializer;

import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.health.config.HealthProperties;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.util.StringUtils;

/**
 * @author: chejiangyi
 * @version: 2019-05-28 12:08
 **/
public class HealthApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext context) {
        ConfigurableEnvironment environment = context.getEnvironment();
        this.initializeSystemProperty(environment);
    }

    void initializeSystemProperty(ConfigurableEnvironment environment) {
        String propertyValue = environment.getProperty(HealthProperties.SpringApplictionName);
        if(propertyValue==null)
        {   return; }

        //默认关闭spring boot 健康信息
        setDefaultProperty(HealthProperties.ManagementEndpointsEnabledByDefault,"false","spring-boot-actuator 默认关闭");

        propertyValue = environment.getProperty(HealthProperties.BsfHealthEnabled);
        if (StringUtils.isEmpty(propertyValue)|| "false".equalsIgnoreCase(propertyValue)) {
            {
                return;
            }
        }

    }
    void setDefaultProperty(String key,String defaultPropertyValue,String message)
    {
        PropertyUtils.setDefaultInitProperty(HealthApplicationContextInitializer.class,HealthProperties.Project,key,defaultPropertyValue,message);
    }
}
