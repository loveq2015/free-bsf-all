package com.free.bsf.health.collect;

import com.free.bsf.core.common.Collector;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.health.base.AbstractCollectTask;
import com.free.bsf.health.base.FieldReport;
import lombok.Data;
import lombok.val;

@Deprecated
/**
 * 代码暂时不使用
 * */
public class WebCollectTask extends AbstractCollectTask {

    public WebCollectTask() {
    }

    @Override
    public int getTimeSpan() {
        return PropertyUtils.getPropertyCache("bsf.health.web.timeSpan", 20);
    }

    @Override
    public String getDesc() {
        return "bsf web性能采集";
    }

    @Override
    public String getName() {
        return "bsf.health.web.info";
    }

    @Override
    public boolean getEnabled() {
        return PropertyUtils.getPropertyCache("bsf.health.web.enabled", true);
    }

    @Override
    protected Object getData() {
        WebCollectTask.WebUrlInfo info = new WebCollectTask.WebUrlInfo();
        val hook = Collector.Default.hook("bsf.web.url.hook");
        if (hook != null) {
            info.hookCurrent = hook.getCurrent();
            info.hookError = hook.getLastErrorPerSecond();
            info.hookSuccess = hook.getLastSuccessPerSecond();
            info.hookList = hook.getMaxTimeSpanList().toText();
            info.hookListPerMinute=hook.getMaxTimeSpanListPerMinute().toText();
        }
        return info;
    }


    @Data
    private static class WebUrlInfo {
        @FieldReport(name = "web.url.hook.error", desc = "web url 拦截上一次每秒出错次数")
        private Long hookError;
        @FieldReport(name = "web.url.hook.success", desc = "web url 拦截上一次每秒成功次数")
        private Long hookSuccess;
        @FieldReport(name = "web.url.hook.current", desc = "web url 拦截当前执行任务数")
        private Long hookCurrent;
        @FieldReport(name = "web.url.hook.list.detail", desc = "web url 拦截历史最大耗时任务列表")
        private String hookList;
        @FieldReport(name = "web.url.hook.list.minute.detail", desc = "web url 拦截历史最大耗时任务列表(每分钟)")
        private String hookListPerMinute;
    }
}