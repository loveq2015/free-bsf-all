package com.free.bsf.health.config;

import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.PropertyUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: chejiangyi
 * @version: 2019-07-23 20:42
 **/
public class WarnProperties {

    /**
     * 报警是否开启
     * @return
     */
    public static boolean getWarnEnabled(){return PropertyUtils.getPropertyCache("bsf.health.warn.enabled",false);}
    /**
     * 报警消息缓存数量
     * @return
     */
    public static int getMaxCacheMessagesCount(){return PropertyUtils.getPropertyCache("bsf.health.warn.cachecount",3);}

    /**
     * 报警消息循环间隔时间 秒
     * @return
     */
    public static int getNotifyTimeSpan(){return PropertyUtils.getPropertyCache("bsf.health.warn.timespan",10);}

    /**
     * 报警重复过滤时间间隔 分钟
     * @return
     */
    public static int getNotifyDuplicateTimeSpan(){return PropertyUtils.getPropertyCache("bsf.health.warn.duplicate.timespan",2);}

    /**
     * sql 执行超时 耗秒
     * @return
     */
    public static int getWarnSqlExecuteTimeOut(){return PropertyUtils.getPropertyCache("bsf.health.warn.sql.execute.timeout",1000);}

    /**
     * sql 执行超时 排除方法，参考:CategoryMapper.getCategoryExistSpec
     * @return
     */
    public static String getWarnSqlExecuteExclude(){return PropertyUtils.getPropertyCache("bsf.health.warn.sql.execute.exclude","");}
    /**
     * sql语句输出
     * @return
     */
    public static boolean getSqlInfo(){return PropertyUtils.getPropertyCache("bsf.health.sql.show.enabled",false);}

    /**
     * sql 执行错误报警
     * @return
     */
    public static boolean getWarnSqlExecuteErrorEnabled(){return PropertyUtils.getPropertyCache("bsf.health.warn.sql.execute.error.enabled",true);}

    /**
     * sql 最大结果集报警
     * @return
     */
    public static int getWarnSqlExecuteResultMaxSize(){return PropertyUtils.getPropertyCache("bsf.health.warn.sql.execute.result.maxSize",10000);}

    /**
     * sql 传入参数集报警
     * @return
     */
    public static int getWarnSqlExecuteParameterMaxSize(){return PropertyUtils.getPropertyCache("bsf.health.warn.sql.execute.parameter.maxSize",2000);}
    /**
     * url 执行超时 耗秒
     * @return
     */
    public static int getWarnUrlExecuteTimeOut(){return PropertyUtils.getPropertyCache("bsf.health.warn.url.execute.timeout",10000);}

    /**
     * url 执行超时 排除方法，参考:/aaa/bbb
     * @return
     */
    public static String getWarnUrlExecuteExclude(){return PropertyUtils.getPropertyCache("bsf.health.warn.url.execute.exclude","");}

    /**
     * url 执行错误报警，排除特定类型
     * @return
     */
    public static String getWarnUrlExecuteErrorExcludeClasses(){return PropertyUtils.getPropertyCache("bsf.health.warn.url.execute.error.exclude.classes","");}

    /**
     * url 执行错误报警
     * @return
     */
    public static boolean getWarnUrlExecuteErrorEnabled(){return PropertyUtils.getPropertyCache("bsf.health.warn.url.execute.error.enabled",true);}
    /**
     * 日志输出内容限制长度，默认500
     * @return
     */
    public static int getMessageContentMaxLength(){return PropertyUtils.getPropertyCache("bsf.health.warn.message.content.max.length",500);}
    /**
     * 日志输出标题限制长度，默认100
     * @return
     */
    public static int getMessageTitleMaxLength(){return PropertyUtils.getPropertyCache("bsf.health.warn.message.title.max.length",100);}
}
