package com.free.bsf.health.filter;

import com.free.bsf.core.base.BsfApplicationStatus;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.health.dump.DumpProvider;
import lombok.val;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 状态服务
 * @author: chejiangyi
 * @version: 2019-09-07 13:31
 **/
public class StatusFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        val request = (HttpServletRequest) servletRequest;
        val response = (HttpServletResponse) servletResponse;
        val contextPath = org.springframework.util.StringUtils.trimTrailingCharacter(request.getContextPath(),'/');
        val uri = request.getRequestURI();
        String path = contextPath+"/bsf/health/status/";
        if(uri.startsWith(path)){
            if(uri.startsWith(path+"get/")) {
                writeText(response,BsfApplicationStatus.Current.name());
            }else if(uri.startsWith(path+"isRunning/")) {
                if(!BsfApplicationStatus.StatusEnum.RUNNING.equals(BsfApplicationStatus.Current)) {
                    response.setStatus(404);
                    response.getWriter().flush();
                    response.getWriter().close();
                }else {
                    writeText(response,BsfApplicationStatus.Current.name());
                }
            }
        }else{
            chain.doFilter(servletRequest,servletResponse);
        }
    }

    private void writeText(HttpServletResponse response,String text) throws IOException{
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().append(text);
        response.getWriter().flush();
        response.getWriter().close();
    }
    @Override
    public void destroy() {

    }
}
