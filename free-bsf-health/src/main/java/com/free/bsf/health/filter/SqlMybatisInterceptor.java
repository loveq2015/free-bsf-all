package com.free.bsf.health.filter;

import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.common.Collector;
import com.free.bsf.core.util.ExceptionUtils;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.MybatisUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.health.base.AbstractCollectTask;
import com.free.bsf.health.base.EnumWarnType;
import com.free.bsf.health.config.HealthProperties;
import com.free.bsf.health.config.WarnProperties;
import com.free.bsf.health.memoryParse.MemoryRecordUtils;
import com.free.bsf.health.memoryParse.ObjParser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.val;

import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.lang.reflect.Array;
import java.util.*;

/**
 * @author: chejiangyi
 * @version: 2019-08-02 09:43
 **/
@Intercepts({
        @Signature(method = "query", type = Executor.class, args = { MappedStatement.class, Object.class,
                RowBounds.class, ResultHandler.class }),
        @Signature(method = "query", type = Executor.class, args = { MappedStatement.class, Object.class,
                RowBounds.class, ResultHandler.class, CacheKey.class,BoundSql.class}),
        @Signature(method = "update", type = Executor.class, args = { MappedStatement.class, Object.class }) })
public class SqlMybatisInterceptor implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
        // 得到sql语句
        Object parameter = null;
        if (invocation.getArgs().length > 1) {
            parameter = invocation.getArgs()[1];
        }
        BoundSql boundSql = mappedStatement.getBoundSql(parameter);
        String sql = boundSql.getSql();
        try {
            printSqlLog(mappedStatement,boundSql);
            checkParameterSize(mappedStatement,boundSql);
            Object returnObj = Collector.Default.hook("bsf.mybatis.sql.hook").run(StringUtils.nullToEmpty(sql).replace("\r","").replace("\n",""), () -> {
                try {
                    var s = System.currentTimeMillis();
                    var r = invocation.proceed();
                    var t=System.currentTimeMillis()-s;
                    checkSqlExecuteTime(mappedStatement,boundSql,t);
                    return r;
                } catch (Exception e) {
                    warnSqlError(mappedStatement,boundSql,e);
                    throw new BsfException(e);
                }
            });
            checkReturnValueSize(mappedStatement,boundSql,returnObj);
            return returnObj;
        }catch (BsfException exp)
        {
            throw exp.getSource();
        }
    }

    private void printSqlLog(MappedStatement mappedStatement, BoundSql boundSql) {
        if(!WarnProperties.getSqlInfo()){
            return;
        }
        val sqlInfo = hitCondition(mappedStatement,boundSql);
        if(sqlInfo != null){
            LogUtils.info(SqlMybatisInterceptor.class, HealthProperties.Project, "【SQL日志输出】" + "\r\n" + sqlInfo.sql);
        }
    }

    //sql 执行超时检查
    private void checkSqlExecuteTime(MappedStatement mappedStatement,BoundSql boundSql, long timeSpan){
        if(timeSpan > WarnProperties.getWarnSqlExecuteTimeOut()){
            val sqlInfo = hitCondition(mappedStatement,boundSql);
            if(sqlInfo==null){
                return;
            }
            String sql = MybatisUtils.showSql(mappedStatement.getConfiguration(), boundSql);
            AbstractCollectTask.notifyMessage(EnumWarnType.WARN, "sql执行超时", String.format("[耗时]%sms,[method]%s,[sql]%s",timeSpan,sqlInfo.sqlMethod,sqlInfo.sql));
        }
    }
    //sql 执行出错
    private void warnSqlError(MappedStatement mappedStatement,BoundSql boundSql, Exception e){
        if(WarnProperties.getWarnSqlExecuteErrorEnabled()){
            val sqlInfo = hitCondition(mappedStatement,boundSql);
            if(sqlInfo==null){
                return;
            }
            AbstractCollectTask.notifyMessage(EnumWarnType.WARN, "sql执行错误", String.format("[method]%s,[sql]%s,[错误]%s",sqlInfo.sqlMethod,sqlInfo.sql,ExceptionUtils.getDetailMessage(e)));
        }
    }
    //结果集检测
    private void checkReturnValueSize(MappedStatement mappedStatement,BoundSql boundSql,Object returnValue){
        if(WarnProperties.getWarnSqlExecuteResultMaxSize()>0){
            val objSize = getObjectSize(returnValue);
            if(objSize>WarnProperties.getWarnSqlExecuteResultMaxSize()){
                val sqlInfo = hitCondition(mappedStatement,boundSql);
                if(sqlInfo==null){
                    return;
                }
                AbstractCollectTask.notifyMessage(EnumWarnType.WARN, "sql结果集过大", String.format("[method]%s,[sql]%s,[结果集大小]%s",sqlInfo.sqlMethod,sqlInfo.sql,objSize));
            }
        }
    }

    //sql参数数量检测
    private void checkParameterSize(MappedStatement mappedStatement,BoundSql boundSql){
        if(WarnProperties.getWarnSqlExecuteParameterMaxSize()>0){
            val ps = MybatisUtils.getParameterObjects(mappedStatement.getConfiguration(),boundSql);
            int objSize = 0;
            for(val p:ps) {
                objSize+=getObjectSize(p);
            }
            if (objSize > WarnProperties.getWarnSqlExecuteParameterMaxSize()) {
                val sqlInfo = hitCondition(mappedStatement, boundSql);
                if (sqlInfo == null) {
                    return;
                }
                AbstractCollectTask.notifyMessage(EnumWarnType.WARN, "sql参数集过大", String.format("[method]%s,[sql]%s,[参数集大小]%s", sqlInfo.sqlMethod, sqlInfo.sql,objSize));
            }
        }
    }

    private SqlInfo hitCondition(MappedStatement mappedStatement,BoundSql boundSql){
        String sqlMethod = StringUtils.nullToEmpty(MybatisUtils.getSqlMethodName(mappedStatement));
        if(StringUtils.hitCondition(WarnProperties.getWarnSqlExecuteExclude().toLowerCase(),sqlMethod.toLowerCase()))
            return null;
        String sql = MybatisUtils.showSql(mappedStatement.getConfiguration(), boundSql);
        SqlInfo sqlInfo = new SqlInfo(sqlMethod,sql);
        return sqlInfo;
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
    }

    protected int getObjectSize(Object obj){
        if(obj==null)
            return 0;
        Class cls=obj.getClass();
        if (cls.isArray()) {
            return Array.getLength(obj);
        } else if (obj instanceof Collection) {
            return ((Collection) obj).size();
        }else if(obj instanceof Map){
            return  ((Map) obj).size();
        }
        return 1;
    }
    @Data
    @AllArgsConstructor
    private static class SqlInfo{
        String sqlMethod;
        String sql;
    }

}