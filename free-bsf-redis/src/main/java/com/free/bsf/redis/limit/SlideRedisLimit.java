package com.free.bsf.redis.limit;

import com.free.bsf.core.util.ConvertUtils;
import lombok.val;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class SlideRedisLimit  extends BaseRedisLimit {
    public SlideRedisLimit(Object jedis){
        super(jedis);
    }

    private static final String lua=
            "local count = tonumber(ARGV[1])   ---第一个参数 次数\n" +
            "local curr_time_arr = redis.call('TIME') \n" +
            "local time = (curr_time_arr[1]*1000000+curr_time_arr[2])/1000 \n"+
            "local period = tonumber(ARGV[2])    ---第二个参数 间隔时间\n" +
            "redis.call('ZREMRANGEBYSCORE', KEYS[1], 0, tonumber(time) - tonumber(period)); ---移除时间窗外的值\n" +
            "if (redis.call('ZCARD', KEYS[1]) >= tonumber(count)) then   ---\n" +
            "    return 0;\n" +
            "end ;\n" +
            "redis.call('ZADD', KEYS[1], time, time);\n" +
            "redis.call('pexpire',KEYS[1], period);\n" +
            "return 1;\n";
    @Override
    public boolean visit(String key, int second, int count) {
        val keys = this.redisListParams(key);
        val args = this.redisListParams(count+"",TimeUnit.MILLISECONDS.convert(second, TimeUnit.SECONDS)+"");
        val number = this.eval(lua,keys,args);
        if(ConvertUtils.convert(number,Integer.class)==1){
            return true;
        }
        return false;
    }
}
