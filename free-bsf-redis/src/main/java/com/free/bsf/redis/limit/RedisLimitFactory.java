package com.free.bsf.redis.limit;

import redis.clients.jedis.Jedis;

public class RedisLimitFactory {
    public static BaseRedisLimit create(Object jedis, RedisLimitType redisLimitType){
        if(redisLimitType== RedisLimitType.Count)
            return new CountRedisLimit(jedis);
        else if(redisLimitType == RedisLimitType.Slide)
            return new SlideRedisLimit(jedis);
        else if(redisLimitType == RedisLimitType.Token)
            return new TokenRedisLimit(jedis);
        return null;
    }
}
