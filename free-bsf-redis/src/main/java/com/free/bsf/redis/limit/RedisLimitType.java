package com.free.bsf.redis.limit;

public enum RedisLimitType {
    Count("计数"),
    Slide("滑动"),
    Token("令牌");

    public String Description;
    RedisLimitType(String description){
        this.Description=description;
    }
}
