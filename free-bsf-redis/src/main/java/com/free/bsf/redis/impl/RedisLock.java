package com.free.bsf.redis.impl;

import com.free.bsf.core.common.Collector;
import com.free.bsf.core.util.ExceptionUtils;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.WarnUtils;
import com.free.bsf.redis.RedisException;
import com.free.bsf.redis.config.RedisProperties;
import lombok.val;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.exceptions.JedisClusterMaxAttemptsException;
import redis.clients.jedis.params.SetParams;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.LockSupport;

/**
 * @author Huang Zhaoping
 */
public final class RedisLock implements Lock {
    private static final String SCRIPT_SUCCESS = "1";
    private static final String UNLOCK_SCRIPT = "if redis.call(\"get\",KEYS[1]) == ARGV[1] then return redis.call(\"del\",KEYS[1]) else return 0 end";
    private static final String RENEW_SCRIPT = "if redis.call(\"get\",KEYS[1]) == ARGV[1] then return redis.call(\"expire\",KEYS[1],${expire}) else return 0 end";

    private final String clientId = UUID.randomUUID().toString();
    private final AbstractRedisProvider redisProvider;
    //private final SetParams lockParam;
    private final String lockKey;
    private final int timeout;
    private static final int DEFAULT_TIMEOUT = 60;
    private final int renewTimeout;
    private final boolean autoRenew;

    public RedisLock(AbstractRedisProvider redisProvider, String key, int timeout,int renewTimeout) {
        if (key == null || (key = key.trim()).length() == 0) {
            throw new RedisException("Redis锁的Key不能为空");
        }
        this.timeout = timeout <= 0 ? DEFAULT_TIMEOUT : timeout;
        //续约周期超过超时时间一半
        if(renewTimeout>(this.timeout/2))
            renewTimeout=this.timeout/2;
        this.renewTimeout= renewTimeout <= 0?0:renewTimeout;
        this.autoRenew = renewTimeout > 0;
        this.redisProvider = redisProvider;
        this.lockKey = key;
        //this.lockParam = this.timeout;//SetParams.setParams().ex(this.timeout).nx();
    }

    public String getClientId() {
        return clientId;
    }

    public int getRenewTimeout() {
        return renewTimeout;
    }

    @Override
    public void lock() {
        try {
            doLock(0L, false);
        } catch (InterruptedException e) {
            throw new RedisException("获取Redis锁被中断");
        }
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        //doLock(0L, true);
    }

    @Override
    public boolean tryLock() {
        return doLock();
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        //return doLock(unit.toNanos(time), true);
        return false;
    }

    @Override
    public void unlock() {
        if (!doUnlock()) {
            RedisException e = new RedisException("Redis分布式锁释放异常，Key：" + lockKey + "，Client：" + clientId);
            String content = ExceptionUtils.trace2String(e);
            Collector.Default.value("jedis.cluster.lock.error.detail").set(content);
            WarnUtils.notifynow("ERROR", "Redis分布式锁释放异常", content);
            throw e;
        }
    }

    @Override
    public Condition newCondition() {
        throw new UnsupportedOperationException("Redis分布式锁不支持newCondition操作");
    }

    private boolean doLock() {
        boolean success = false;
        int retryAttempts = 3;
        do {
            try {
                success = redisProvider.setIfAbsent(lockKey, clientId, this.timeout);
            } catch (JedisClusterMaxAttemptsException e) {
                // retry
            }
        } while (!success && retryAttempts-- > 0);
        if (success&&autoRenew) {
            RedisLockRenew.add(this);
        }
        return success;
    }

    private boolean doLock(long waitNanos, boolean interruptibly) throws InterruptedException {
        long deadline = waitNanos <= 0 ? Long.MAX_VALUE : System.nanoTime() + waitNanos;
        while (!doLock()) {
            if (deadline > System.nanoTime()) {
                LockSupport.parkNanos(100000000L);// 等100毫秒
                if (interruptibly && Thread.interrupted()) {
                    throw new InterruptedException();
                }
            } else {
                return false;
            }
        }
        return true;
    }

    private boolean doUnlock() {
        List<String> keys = Arrays.asList(lockKey);
        List<String> args = Arrays.asList(clientId);
        Object result = redisProvider.eval(UNLOCK_SCRIPT, keys, args);
        if (autoRenew) {
            RedisLockRenew.remove(this);
        }
        return SCRIPT_SUCCESS.equalsIgnoreCase(String.valueOf(result));
    }

    protected boolean doRenew() {
        List<String> keys = Arrays.asList(lockKey);
        List<String> args = Arrays.asList(clientId);
        //续约时间+5秒
        val time = this.renewTimeout+5;
        Object result = redisProvider.eval(RENEW_SCRIPT.replace("${expire}",time+""), keys, args);
        Boolean success=SCRIPT_SUCCESS.equalsIgnoreCase(String.valueOf(result));
        if(success) {
            LogUtils.debug(this.getClass(), RedisProperties.Project, "分布式锁key:"+lockKey+"续租成功,clientid:" +clientId+",续约延长:"+time+"s,续约周期:"+renewTimeout+"s");
        }
        return success;
    }
}
