## Redis进阶配置 

### 介绍
进阶配置涉及到redis配置的优化,一般开发人员无需关心和关注，标准优化方式都已做好。

### 进阶配置
```
  #bsf redis 默认值:cluster;可以为cluster(集群),jedis(单机版)
  bsf.redis.type=cluster
  #bsf redis 默认值:项目名去掉free-,-provider,-task;特殊情况业务需要定制,避免冲突和可阅读		
  bsf.redis.keyPrefix=	
  #bsf redis 服务器集群地址{ip}:{port}，逗号隔开;必填;非集群版填一个即可。
  bsf.redis.nodes = XX.XX.XX.XX:6379
  #bsf redis 连接池【优化】-最大连接池大小
  bsf.redis.pool.maxTotal = 30
  #bsf redis 连接池【优化】-最大空闲池大小
  bsf.redis.pool.maxIdle = 30
  #bsf redis 连接池【优化】-最小空闲池大小
  bsf.redis.pool.minIdle = 0
  #bsf redis 连接池【优化】-接入验证链接可用性
  bsf.redis.pool.testOnBorrow = false
  #bsf redis 连接池【优化】-空闲时验证链接可用性
  bsf.redis.pool.testWhileIdle = true
  #bsf redis 连接池【优化】-使用完毕时验证链接可用性
  bsf.redis.pool.testOnReturn = false
  #bsf redis 连接池【优化】-创建链接时验证链接可用性
  bsf.redis.pool.testOnCreate = false
  #bsf redis 连接池【优化】-空闲有效时间(超过这个时间启用testWhileIdle的验证有效性)
  bsf.redis.pool.timeBetweenEvictionRunsMillis = 30000
  #bsf redis 连接池【优化】-最小空闲时间(超过这个时间则开始考虑回收)
  bsf.redis.pool.minEvictableIdleTimeMillis = 60000
  bsf.redis.password = 
  #redis 连接超时时间	
  bsf.redis.connectTimeout = 5000
  bsf.redis.soTimeout = 60000
  #redis 最大重试次数；若非集群版本此参数无效
  bsf.redis.maxAttempts = 5
  bsf.redis.clientName=	
```